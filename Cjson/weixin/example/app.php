<?php
ini_set('date.timezone','Asia/Shanghai');
error_reporting(E_ERROR);
require_once "../lib/WxPay.Api.php";
require_once "WxPay.AppPay.php";
$notify = new AppPay();
/*首先生成prepayid*/
$money=$_REQUEST['money']*100;
$title=$_REQUEST['title'];
$order_list='';
$out_trade_no=WxPayConfig::MCHID.date("YmdHis");
if(isset($order_list)) $order_list=$_REQUEST['order_list'];
$type='order';
$user_id=0;//可选参数
$goods_id=0;//可选参数
$address=0;//可选参数
$url='http://dsh.yao2099.com/Cjson/WxPay_return.php';
if(isset($_REQUEST['type'])) $type=$_REQUEST['type'];
if(isset($_REQUEST['user_id'])) $user_id=$_REQUEST['user_id'];
if(isset($_REQUEST['goods_id'])) $goods_id=$_REQUEST['goods_id'];
if(isset($_REQUEST['address'])) $address=$_REQUEST['address'];
if($type=='jf_order')
{
	$url='http://dsh.yao2099.com/Cjson/WxPay_return_jf_order.php';
	$order_list=$user_id.','.$goods_id.','.$address;
} 
if($type=='chongzhi')
{
	$url='http://dsh.yao2099.com/Cjson/WxPay_return_chongzhi.php';
	$order_list=$user_id;
} 
if($type=='yyg')
{
	$url='http://dsh.yao2099.com/Cjson/WxPay_return_yyg.php';
	$order_list=$user_id.'_'.$goods_id;
}
file_put_contents("tsxx_WxPay_app.txt", "\r\n--------------------------\r\n", FILE_APPEND);
file_put_contents("tsxx_WxPay_app.txt", "\r\n调用app签名等接口:".date('Y-m-d H:i:s')."\r\n", FILE_APPEND);
file_put_contents("tsxx_WxPay_app.txt", "\r\nmoney    ：".$money."\r\n", FILE_APPEND);
file_put_contents("tsxx_WxPay_app.txt", "\r\ntitle    ：".$title."\r\n", FILE_APPEND);
file_put_contents("tsxx_WxPay_app.txt", "\r\norder_list    ：".$order_list."\r\n", FILE_APPEND);
file_put_contents("tsxx_WxPay_app.txt", "\r\ntype    ：".$type."\r\n", FILE_APPEND);
file_put_contents("tsxx_WxPay_app.txt", "\r\nuser_id    ：".$user_id."\r\n", FILE_APPEND);
file_put_contents("tsxx_WxPay_app.txt", "\r\ngoods_id    ：".$goods_id."\r\n", FILE_APPEND);
file_put_contents("tsxx_WxPay_app.txt", "\r\naddress    ：".$address."\r\n", FILE_APPEND);
file_put_contents("tsxx_WxPay_app.txt", "\r\nurl    ：".$url."\r\n", FILE_APPEND);
file_put_contents("tsxx_WxPay_app.txt", "\r\n--------------------------\r\n", FILE_APPEND);
$input = new WxPayUnifiedOrder();
$input->SetBody($title);//商品或支付单简要描述(必须填写)
$input->SetAttach($order_list);//附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据(不必填)
//$input->SetDetail("Ipad mini  16G  白色,黑色");//商品名称明细列表(不必填)
$input->SetOut_trade_no($out_trade_no);//订单号(必须填写)
$input->SetTotal_fee($money);//订单金额(必须填写)
//$input->SetTime_start(date("YmdHis"));//交易起始时间(不必填)
//$input->SetTime_expire(date("YmdHis",time()+600));//交易结束时间10分钟之内不必填)
$input->SetGoods_tag("test");//商品标记(不必填)
$input->SetNotify_url($url);//回调URL(必须填写)
$input->SetTrade_type("APP");//交易类型(必须填写)
//$input->SetProduct_id("123456789");//rade_type=NATIVE，此参数必传。此id为二维码中包含的商品ID，商户自行定义。
$order = WxPayApi::unifiedOrder($input);//获得订单的基本信息，包括prepayid
$appApiParameters = $notify->GetAppApiParameters($order);//生成提交给app的一些参数
$appApiParameters=json_decode($appApiParameters,true);
$appApiParameters['out_trade_no']=$out_trade_no;
$appApiParameters=json_encode($appApiParameters);
die($appApiParameters);
?>