<?php 
require('config.php');
$uptypes=array('image/jpg', //上传文件类型列表 
'image/jpeg', 
'image/png', 
'image/pjpeg', 
'image/gif', 
'image/bmp', 
'image/x-png'); 
$max_file_size=5000000; //上传文件大小限制, 单位BYTE 
$time2=date('Y-m');
$destination_folder="../Upload/users/".$time2.'/'; //上传文件路径 
$destination_folder2="Upload/users/".$time2.'/'; //存数据库路径 
$watermark=0; //是否附加水印(1为加水印,其他为不加水印); 
$watertype=1; //水印类型(1为文字,2为图片) 
$waterposition=1; //水印位置(1为左下角,2为右下角,3为左上角,4为右上角,5为居中); 
$waterstring=""; //水印字符串 
$waterimg="xplore.gif"; //水印图片 
$imgpreview=1; //是否生成预览图(1为生成,其他为不生成); 
$imgpreviewsize=1/2; //缩略图比例 
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{ 
	if (!is_uploaded_file($_FILES["file"]['tmp_name'])) 
	//是否存在文件 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="文件不存在！";
		echo json_encode($data);
		exit; 
	} 
	$file = $_FILES["file"]; 
	if($max_file_size < $file["size"]) 
	//检查文件大小 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="文件太大！";
		echo json_encode($data);
		exit; 
	} 
	if(!in_array($file["type"], $uptypes)) 
	//检查文件类型 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="只能上传图像文件！";
		echo json_encode($data);
		exit; 
	} 
	if(!file_exists($destination_folder)) mkdir($destination_folder); 
	$filename=$file["tmp_name"]; 
	$image_size = getimagesize($filename); 
	$pinfo=pathinfo($file["name"]); 
	$ftype=$pinfo['extension']; 
	$destination = $destination_folder.time().".".$ftype; 
	if (file_exists($destination) && $overwrite != true) 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="同名文件已经存在了！";
		echo json_encode($data);
		exit; 
	} 
	if(!move_uploaded_file ($filename, $destination)) 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="移动文件出错！";
		echo json_encode($data);
		exit; 
	} 
	$pinfo=pathinfo($destination); 
	$fname=$pinfo['basename']; 
	if(isset($_REQUEST['user_id'])) $user_id=$_REQUEST['user_id'];
	else $user_id=0;
	$src=$destination_folder2.$fname;
	$sql="update ".$oto."_users set userPhoto='{$src}' where userId='{$user_id}'";
	$result=$db->query($sql);
	if($result)
	{
		$data[0]['pan']=1;
		$data[0]['msg']=$src;
		echo json_encode($data);
	}
	else
	{
		$data[0]['pan']=-1;
		$data[0]['msg']="上传失败！";
		echo json_encode($data);
	}
	// if($watermark==1) 
	// { 
	// 	$iinfo=getimagesize($destination,$iinfo); 
	// 	$nimage=imagecreatetruecolor($image_size[0],$image_size[1]); 
	// 	$white=imagecolorallocate($nimage,255,255,255); 
	// 	$black=imagecolorallocate($nimage,0,0,0); 
	// 	$red=imagecolorallocate($nimage,255,0,0); 
	// 	imagefill($nimage,0,0,$white); 
	// 	switch ($iinfo[2]) 
	// 	{ 
	// 		case 1: 
	// 		$simage =imagecreatefromgif($destination); 
	// 		break; 
	// 		case 2: 
	// 		$simage =imagecreatefromjpeg($destination); 
	// 		break; 
	// 		case 3: 
	// 		$simage =imagecreatefrompng($destination); 
	// 		break; 
	// 		case 6: 
	// 		$simage =imagecreatefromwbmp($destination); 
	// 		break; 
	// 		default: 
	// 		die("不能上传此类型文件！"); 
	// 		exit; 
	// 	} 
	// 	imagecopy($nimage,$simage,0,0,0,0,$image_size[0],$image_size[1]); 
	// 	imagefilledrectangle($nimage,1,$image_size[1]-15,80,$image_size[1],$white); 
	// 	switch($watertype) 
	// 	{ 
	// 		case 1: //加水印字符串 
	// 		imagestring($nimage,2,3,$image_size[1]-15,$waterstring,$black); 
	// 		break; 
	// 		case 2: //加水印图片 
	// 		$simage1 =imagecreatefromgif("xplore.gif"); 
	// 		imagecopy($nimage,$simage1,0,0,0,0,85,15); 
	// 		imagedestroy($simage1); 
	// 		break; 
	// 	} 
	// 	switch ($iinfo[2]) 
	// 	{ 
	// 		case 1: 
	// 		//imagegif($nimage, $destination); 
	// 		imagejpeg($nimage, $destination); 
	// 		break; 
	// 		case 2: 
	// 		imagejpeg($nimage, $destination); 
	// 		break; 
	// 		case 3: 
	// 		imagepng($nimage, $destination); 
	// 		break; 
	// 		case 6: 
	// 		imagewbmp($nimage, $destination); 
	// 		//imagejpeg($nimage, $destination); 
	// 		break; 
	// 	} 
	// 	//覆盖原上传文件 
	// 	imagedestroy($nimage); 
	// 	imagedestroy($simage); 
	// } 
// if($imgpreview==1) 
// { 
// echo "<br>图片预览:<br>"; 
// echo "<a href=\"".$destination."\" target='_blank'><img src=\"".$destination."\" width=".($image_size[0]*$imgpreviewsize)." height=".($image_size[1]*$imgpreviewsize); 
// echo " alt=\"图片预览:\r文件名:".$destination."\r上传时间:\" border='0'></a>"; 
// } 
} 
?>