<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo ($CONF['shopTitle']['fieldValue']); ?>后台管理中心</title>
      <link href="/Public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/Tpl/Admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
      <!--[if lt IE 9]>
      <script src="/Public/js/html5shiv.min.js"></script>
      <script src="/Public/js/respond.min.js"></script>
      <![endif]-->
      <script src="/Public/js/jquery.min.js"></script>
      <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/Public/js/common.js"></script>
      <script src="/Public/plugins/plugins/plugins.js"></script>
   </head>
   <script>
   function getAreaList(objId,parentId,t,id){
	   var params = {};
	   params.parentId = parentId;
	   $('#'+objId).empty();
	   if(t<1){
		   $('#areaId3').empty();
		   $('#areaId3').html('<option value="">请选择</option>');
	   }
	   var html = [];
	   $.post("<?php echo U('Admin/Areas/queryByList');?>",params,function(data,textStatus){
		    html.push('<option value="">请选择</option>');
			var json = WST.toJson(data);
			if(json.status=='1' && json.list.length>0){
				var opts = null;
				for(var i=0;i<json.list.length;i++){
					opts = json.list[i];
					html.push('<option value="'+opts.areaId+'" '+((id==opts.areaId)?'selected':'')+'>'+opts.areaName+'</option>');
				}
			}
			$('#'+objId).html(html.join(''));
	   });
   }
   $(function(){
       <?php if($areaId1!=0){ ?>
	   getAreaList("areaId2",'<?php echo ($areaId1); ?>',0,'<?php echo ($areaId2); ?>');
	   <?php } ?>  
	   <?php if($areaId2 != 0){ ?>
	   getAreaList("areaId3",'<?php echo ($areaId2); ?>',1,'<?php echo ($areaId3); ?>');
	   <?php } ?>
	   $('#orderStatus').val(<?php echo ($orderStatus); ?>);
   });



   function pay(orderId){

       Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
       $.post("<?php echo U('Admin/Agent/orderAction');?>",{orderId:orderId},function(data){
          var json = WST.toJson(data);

           if(json.status==true){
               Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
                   location.reload();

               }});
           }
           else{

               Plugins.closeWindow();
               Plugins.Tips({title:'信息提示',icon:'error',content:json.error,timeout:1000});
           }


       });



   }




   </script>
   <body class='wst-page'>

       <div class="wst-body"> 
        <table class="table table-hover table-striped table-bordered wst-list">
           <?php if(is_array($Page['root'])): $key = 0; $__LIST__ = $Page['root'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($key % 2 );++$key;?><thead>
             <tr>
               <th colspan='6'><?php echo ($key); ?>.订单：<?php echo ($vo['orderNo']); ?><span style='margin-left:100px;'><a href="<?php echo U('Home/Shops/toShopHome',array('shopId'=>$vo['shopId']));?>" target='_blank'><?php echo ($vo['shopName']); ?></a></span></th>
             </tr>
           </thead>
           <tbody>
             <tr>
               <td>
               <div style='width:150px;'>
               <?php if(is_array($vo['goodslist'])): $i = 0; $__LIST__ = $vo['goodslist'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$goods): $mod = ($i % 2 );++$i;?><img style='margin:2px;' src="/<?php echo ($goods['goodsThums']); ?>" height="50" width="50" title='<?php echo ($goods['goodsName']); ?>'/><?php endforeach; endif; else: echo "" ;endif; ?>
			   </div>
               </td>
               <td><?php echo ($vo['userName']); ?></td>
               <td>
                                             ￥<?php echo ($vo['totalMoney']+$vo['deliverMoney']); ?><br/>
                   <?php if($vo['payType'] ==0 ): ?>货到付款
                       <?php elseif($vo["payType"] == 1): ?>支付宝
                       <?php elseif($vo["payType"] == 2): ?>微信
                       <?php elseif($vo["payType"] == 3): ?>余额
                       <?php elseif($vo["payType"] == 4): ?>积分<?php endif; ?>
               </td>
               <td><?php echo ($vo['createTime']); ?></td>
               <td>
               <?php if($vo["orderStatus"] == -3): ?>用户拒收
               <?php elseif($vo["orderStatus"] == -5): ?>店铺不同意拒收
               <?php elseif($vo["orderStatus"] == -4): ?>店铺同意拒收
			   <?php elseif($vo["orderStatus"] == -2): ?>未付款
			   <?php elseif(($vo["orderStatus"] == -6) OR ($vo["orderStatus"] == -7) OR ($vo["orderStatus"] == -1)): ?>用户取消
			   <?php elseif($vo["orderStatus"] == 0): ?>未受理
			   <?php elseif($vo["orderStatus"] == 1): ?>已受理
			   <?php elseif($vo["orderStatus"] == 2): ?>打包中
			   <?php elseif($vo["orderStatus"] == 3): ?>配送中
			   <?php elseif($vo["orderStatus"] == 4): ?>已到货<?php endif; ?>
               </td>

                 <td>
                     <?php if($vo["isAgent"] == 1): ?>已分成<?php endif; ?>
                     <?php if($vo["isAgent"] == 0): ?>未分成<?php endif; ?>
                 </td>
               <td>
               <a class="btn btn-primary glyphicon" href="<?php echo U('Admin/Agent/orderToView',array('id'=>$vo['orderId']));?>"">查看</a>&nbsp;
                   <button class="btn btn-warning glyphicon" onclick="javascript:pay(<?php echo ($vo['orderId']); ?>)">分成</button>&nbsp;
               </td>
             </tr><?php endforeach; endif; else: echo "" ;endif; ?>
             <tr>
                <td colspan='6' align='center'><?php echo ($Page['pager']); ?></td>
             </tr>
           </tbody>
        </table>
       </div>
   </body>
</html>