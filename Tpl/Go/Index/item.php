<include file="Common:public_header" />
<link href="<?php echo MOBILE_TPL_PATH;?>/css/goods.css" rel="stylesheet" type="text/css" />
<?php if($shopitem=='itemfun'){ ?>
    <script id="pageJS" data="<?php echo MOBILE_TPL_PATH;?>/js/goodsInfo.js" language="javascript" type="text/javascript"></script>
<?php }else{?>
	<script id="pageJS" data="<?php echo MOBILE_TPL_PATH;?>/js/LotteryDetail.js" language="javascript" type="text/javascript"></script>
<?php }?>
<style>
body{
	position:inherit;
}
.ddradius{position:absolute;bottom:8px;width:100%;height:8px;z-index:999;}
.ddradius dd{width:8px;height:8px;display:inline-block;background:#bbb;margin:5px;border-radius:8px;}
.ddradius .selected{background:#666;}
.pImg .direction-nav li{background:#fff;opacity:1;}
.pResultsR .g-snav-lst{height:75px;}
</style>
</head>
<body>
<div class="h5-1yyg-v1" id="loadingPicBlock">
<!-- 内页顶部 -->
<header class="bar bar-nav" id="header">
  <a class="icon icon-left-nav pull-left" href="javascript:;"  onclick="history.go(-1)"></a>
<?php if ($item['q_end_time']){?>
  <h1 class="title">揭晓结果</h1>
<?php }else{?>
  <h1 class="title">商品详情</h1> 
<?php }?>
</header>
<!-- 内页顶部 -->

    <input name="hidGoodsID" type="hidden" id="hidGoodsID" value="<?php if ($itemlist){echo $itemlist[0]['q_uid'];}?>"/>
    <input name="hidCodeID" type="hidden" id="hidCodeID" value="<?php echo $item['id'];?>"/>
    	    <!-- 导航 -->
        <div id="divPeriod" class="pNav" style="top:44px;">
            <div class="loading"><b></b>正在加载</div>
    	    <ul class="slides">
    	      <?php echo $loopqishu;?>
            </ul>
        </div>
	<div id="wrapper" style="top:80px;">
	<section class="goodsCon pCon" id="scroller">


		<?php $sysj=$item['xsjx_time']-time();?>


        <!-- 揭晓信息 -->
        <?php if ($item['q_end_time']!='' && $item['q_end_time'] <= time()){ ?>
        <div class="pProcess pProcess2" style="margin:10px 8px;">
    	    <div class="pResults">
        	    <div class="pResultsL" onclick="location.href='<?php echo U('User/userindex',array('uid' => $item['q_uid']));?>'">
            	    <a>
            	        <img src="<?php echo C('PIC_URL').$item['uphoto'];?>">
            	        <span><?php echo $item['q_user']['username'];?></span>
            	    </a>
                    <s></s>
                </div>
        	    <div class="pResultsR">
                    <div class="g-snav">
                        <div class="g-snav-lst">总共抢购<br><dd><b class="orange"><?php echo $user_shop_number;?></b><br>人次</dd></div>
                        <div class="g-snav-lst">揭晓时间<br><dd class="gray9"><span><?php echo str_replace(' ','<br>',microt($item['q_end_time']));?></span></dd></div>
                        <div class="g-snav-lst">抢购时间<br><dd class="gray9"><span><?php echo str_replace(' ','<br>',microt($user_shop_time));?></span></dd></div>
                    </div>
                </div>
        	    <p><a href="<?php echo U('Index/calresult',array('gid' => $item['id']));?>" class="fr">查看计算结果</a>幸运抢购码：<b class="orange"><?php echo $item['q_user_code'];?></b></p>
            </div>
        </div>
       <?php } ?>

		<!-- 揭晓倒计时 -->
		<?php if ($item['q_end_time']!='' && $item['q_end_time'] > time()){ ?>
			<div id="divLotteryTime" style="margin:0px 8px;" class="pProcess clearfix" data-id="<?php echo $item['id'];?>" data-endtime="<?php echo ceil($item['q_end_time']-time());?>">
				<div class="pCountdown">
					<div class="g-snav">
						<div class="g-snav-lst">揭晓<br>倒计时<s></s></div>
						<div class="g-snav-lst"><b class="minute">99</b><em>分</em></div>
						<div class="g-snav-lst"><b class="second">99</b><em>秒</em></div>
						<div class="g-snav-lst"><b class="millisecond">99</b><em>毫秒</em></div>
					</div>
				</div>
			</div>
        <?php } ?>
        <!-- 产品图 -->
        <div class="pPic pPicBor" style="padding-top:10px;background:#fff;border-top:none;">
            <div class="pPic2">
    	        <div id="sliderBox" class="pImg">
                   <!-- <div class="loading"><b></b>正在加载</div>-->
                    <ul class="slides">
					<?php foreach ($item['picarr'] as $imgtu){ ?>
					<li><img src="<?php echo C('PIC_URL').$imgtu;?>" class="animClass" /></li>
					<?php } ?>
                    </ul>
                </div>
            </div>
			<?php if ($item['q_end_time']=='' && $item['xsjx_time']!=0){?>
            <span id="spAutoFlag" class="z-limit-tips">限时揭晓</span>
			 <?php } ?>
        </div>

        <!-- 条码信息 -->


        <div class="pDetails <?php if($item['q_end_time']!=''){ ?>pDetails-end<?php }?>" style="border:none;">
                <p>(第<?php echo $item['qishu'];?>期)<?php echo $item['title'];?> <span style="<?php echo $item['title_style'];?>"><?php echo $item['title2'];?></span></p>
                <p class="price">价值：<em class="arial gray">￥<?php echo $item['money'];?></em></p>
			<?php if(empty($item['q_end_time'])){ ?>
			
				<progress max="100" value="<?php echo $item['canyurenshu']/$item['zongrenshu']*100;?>" class="css3">
					<div class="progress"></div>
				</progress>
				<div class="Progress-bar">
					<ul class="Pro-bar-li">
						<li class="P-bar01"><em><?php echo $item['canyurenshu'];?></em>已参与</li>
						<li class="P-bar02"><em><?php echo $item['zongrenshu'];?></em>总需人次</li>
						<li class="P-bar03"><em><?php echo $item['zongrenshu']-$item['canyurenshu'];?></em>剩余</li>
					</ul>
				</div>

			<?php } ?>
			
				<?php if ($item['q_end_time'] !=''){ 
				
					if($item['q_end_time'] > time()){
					
				?>
					
					  <div class="pClosed">正在揭晓</div>
					<?php }else{?>
						<div class="pClosed">本期已揭晓</div>
					<?php }?>
				
				<?php if ($itemxq==1){ ?>
					<div class="pOngoing" codeid="<?php echo $itemzx['id'];?>">第<em class="arial"><?php echo $itemzx['qishu'];?></em>期 正在进行中……<span class="fr"><a href="?/mobile/mobile/item/{wc:$itemzx['id']}" style="color:#fff;">查看详情</a></span></div>
				<?php }?>
				
           	<?php }elseif ($item['zongrenshu']==$item['canyurenshu']){

           		if ($item['xsjx_time']!=0){ 
           		?>
               <div id="divAutoRTime" class="pSurplus" time="{wc:$sysj}" timeAlt="{wc:fun:date('Y-m-d-H',$item['xsjx_time'])}"><p><span>限时揭晓</span>剩余时间：<em>00</em>时<em>00</em>分<em>00</em>秒</p></div>
			  <?php } ?>
               <!--<div class="pClosed">下手慢了！！ 被抢光啦！！</div>-->
		    
              <?php 
				}else{
              
              if ($item['xsjx_time']!=0){ ?>
			  <div id="divAutoRTime" class="pSurplus" time="{wc:$sysj}" timeAlt="{wc:fun:date('Y-m-d-H',$item['xsjx_time'])}"><p><span>限时揭晓</span>剩余时间：<em>00</em>时<em>00</em>分<em>00</em>秒</p></div>
			  <?php } ?>
			 <!--<div id="btnBuyBox" class="pBtn" codeid="{wc:$item['id']}">
                  <a href="javascript:;" class="fl buyBtn">立即1元抢购</a>
                  <a href="javascript:;" class="fr addBtn">加入购物车</a>
			  </div>
			  -->
			<?php } ?>
        </div>
        <!-- 参与记录，商品详细，晒单导航 -->
	
        <div class="joinAndGet">
			<ul class="table-view">
			  <li class="table-view-cell">
			    <a class="navigate-right" href="<?php echo U('Index/buyrecords',array('gid' => $item['id']));?>">所有抢购记录</a>
			  </li>
			  <li class="table-view-cell">
			    <a class="navigate-right" href="<?php echo U('Index/goodsdesc',array('gid' => $item['id']));?>">图文详情</a>
			  </li>

			</ul>
            <!-- 上期获得者 -->
			<?php if(!empty($gorecode)){ ?>
				<ul id="prevPeriod" class="m-round" codeid="<?php echo $gorecode['id'];?>" style="border:none;" uweb="<?php echo $gorecode['q_uid'];?>">
        	    <li class="fl"><s></s><img src="<?php echo C('PIC_URL').$gorecode['q_user']['userPhoto'];?>"/></li>
                <li class="fr"><b class="z-arrow"></b></li>
                <li class="getInfo">
            	    <dd>
					<em class="blue"><a href="<?php echo U('User/userindex',array('uid' => $gorecode['q_uid']));?>" style="display:inline;position:static;"><?php echo get_user_name($gorecode['q_uid']);?></a></em>(<?php echo get_ip($gorecode_time['id'],'ipcity');?>)
					</dd>
                    <dd>总共抢购：<em class="orange arial"><?php echo $gorecode_count;?></em>人次</dd>
                    <dd>幸运抢购码：<em class="orange arial"><?php echo $gorecode['q_user_code'];?></em></dd>
                    <dd>揭晓时间：<?php echo microt($gorecode['q_end_time']);?></dd>
                    <dd>抢购时间：<?php echo microt($gorecode_time['time']);?></dd>
                </li>
            </ul>
			<?php }?>

        </div>
    </section>
</div>

<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH;?>/assets/agile/css/ratchet/css/ratchet.min.css">
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH;?>/assets/agile/css/flat/iconline.css">
<script type="text/javascript" src="<?php echo MOBILE_TPL_PATH; ?>/layer2.1/layer.js"></script>
<style>
.head-shopcart{
	position:relative;
}
.pPic{padding-bottom:5px !important;}
.footer_goods_num{
	display:none;
	font-size:12px;
	background:red;
	border-radius:50%;
	position:absolute;
	top:1px;
	font-style:normal;
	left:42px;
	width:20px;
	height:20px;
	line-height:20px;
	z-index:999;
	color:#fff;
}
nav.bar-tab span.tab-label{margin-top:-1px;}
nav.bar-tab span.icon{font-size:20px;margin-top:1px;}
nav.bar-tab{
	background-image: linear-gradient(to top,#f9f9f9,#ececec);
    background-image: -webkit-linear-gradient(to top,#f9f9f9,#ececec); /* Safari 5.1 - 6.0 */
	background-image: -o-linear-gradient(to top,#f9f9f9,#ececec); /* Opera 11.1 - 12.0 */
	background-image: -moz-linear-gradient(to top,#f9f9f9,#ececec); /* Firefox 3.6 - 15 */
	background-image: linear-gradient(to top,#f9f9f9,#ececec);
	border-color:#c0bfbf;
}
.bar-tab a{
	width:20%;
}
</style>
<?php if ($shopitem=='itemfun'){?>
<footer class="footer">
<div style="bottom: 0px;">
<nav class="bar bar-tab" style="background:#fff;border-top:1px solid #ddd;">
	<div style="width:56%;float:left;">
  	<a class="tab-item" href="<?php echo U('Index/lists');?>">
    	<span class="icon iconline-grid"></span>
		<span class="tab-label">所有商品</span>
  	</a>
  	<a class="tab-item" href="<?php echo U('Index/lottery');?>">
		<span class="icon iconline-bulb"></span>
    	<span class="tab-label">最新揭晓</span>
  	</a>
  	<a class="tab-item head-shopcart" href="<?php echo U('Cart/cartlist');?>">
    	<span class="icon iconline-shop-basket"></span>
		<span class="tab-label">购物车</span>
		<em class="footer_goods_num" style="display:none;">0</em>
	</a>
	</div>
	<div id="btnBuyBox" class="pBtn" style="width:39%;height:50px;margin-top:0px;float:right;" codeid="<?php echo $item['id'];?>">
        <a href="javascript:;" style="width:100%;height:50px;line-height:50px;border-radius:0px;background:#ff1b1b;border:none;" class="fr addBtn">加入购物车</a>
	</div>
</nav>
</div>

</footer>
<?php }elseif ($shopitem == 'dataserverfun'){ ?>
<footer class="footer">
<div style="bottom: 0px;">
<nav class="bar bar-tab" style="height:35px;background:#fff;border-top:1px solid #ddd;">
	<div style="width:60%;float:left;line-hieght:35px;">
		<p style="margin-bottom: 0px;padding-top:8px;">第 (<?php echo $itemzx['qishu'];?>) 期 正在进行中...</p>
	</div>
	<div id="btnBuyBox" class="pBtn" style="width:39%;height:35px;margin-top:0px;float:right;">
        <a href="<?php echo U('item',array('gid' => $itemzx['id']));?>" style="width:100%;height:35px;line-height:35px;border-radius:0px;background:#ff1b1b;border:none;" class="">查看详情</a>
	</div>
</nav>
</div>

</footer>
<?php } ?>
<script type="text/javascript">
var dizhi=location.href;
  var Path = new Object();
  Path.Skin="<?php echo MOBILE_TPL_PATH;?>";
  Path.Webpath = "<?php echo WEB_URL;?>";
  Path.M = "<?php echo MODULE_NAME;?>";
var Base = {head: document.getElementsByTagName("head")[0] || document.documentElement,Myload: function(B, A) {this.done = false;B.onload = B.onreadystatechange = function() {if (!this.done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {this.done = true;A();B.onload = B.onreadystatechange = null;if (this.head && B.parentNode) {this.head.removeChild(B)}}}},getScript: function(A, C) {var B = function() {};if (C != undefined) {B = C}var D = document.createElement("script");D.setAttribute("language", "javascript");D.setAttribute("type", "text/javascript");D.setAttribute("src", A);this.head.appendChild(D);this.Myload(D, B)},getStyle: function(A, B) {var B = function() {};if (callBack != undefined) {B = callBack}var C = document.createElement("link");C.setAttribute("type", "text/css");C.setAttribute("rel", "stylesheet");C.setAttribute("href", A);this.head.appendChild(C);this.Myload(C, B)}};
function GetVerNum() {var D = new Date();return D.getFullYear().toString().substring(2, 4) + '.' + (D.getMonth() + 1) + '.' + D.getDate() + '.' + D.getHours() + '.' + (D.getMinutes() < 10 ? '0': D.getMinutes().toString().substring(0, 1))}
Base.getScript('<?php echo MOBILE_TPL_PATH;?>/js/Bottom.js');
var shareData = {title: "<?php echo '(第'.$item['qishu'].'期)'._htmtocode($item['title']);?>",desc: "<?php echo _htmtocode($item['description']);?>",link: "<?php echo WEB_URL.U('Index/item',array('gid'=>$item['id']));?>",imgUrl: "<?php echo C('PIC_URL').$item['thumb'];?>"};
$(function(){
$.jqScroll('#wrapper');

<?php if($gorecode){ ?>
  $(".blue").click(function(){
	 window.location.href="<?php echo U('User/userindex',array('uid' => $gorecode['q_uid']));?>";
  });

  $(".orange.arial").click(function(){
	 window.location.href="<?php echo U('Index/dataserver',array('gid' => $gorecode['id']));?>";
  });
<?php } ?>

	// 揭晓倒计时
	var divLotteryTime = $('#divLotteryTime');
	if (divLotteryTime.size() > 0 ) {
		var id = divLotteryTime.attr('data-id');
		var minute = divLotteryTime.find('b.minute');
		var second = divLotteryTime.find('b.second');
		var millisecond = divLotteryTime.find('b.millisecond');
		var tips = minute.parent().prev();
		var times = (new Date().getTime()) + 1000 * divLotteryTime.attr('data-endtime');
		var timer = setInterval(function(){
			var time = times - (new Date().getTime());
			if ( time < 1 ) {
				clearInterval(timer);
				tips.css('line-height', '35px').css('color','#F60').html('刷新下，幸运者就是你！');
				minute.parent().remove();
				second.parent().remove();
				millisecond.parent().remove();
				var checker = function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo U('Ajax/lottery_shop_set');?>',
						data : {
							lottery_sub : true,
							gid : id,
						},
						success : function(info){
							if (info==0) {
								tips.html('刷新下，幸运者就是你');
								//setTimeout(checker,1000);
							} else {
								tips.html('揭晓成功！');
								setTimeout(function(){
									//location.reload();
								},200);
							}
						}
					});
				};
				setTimeout(checker,750);
				return;
			}

			i =  parseInt((time/1000)/60);
			s =  parseInt((time/1000)%60);
			ms =  String(Math.floor(time%1000));
			ms = parseInt(ms.substr(0,2));
			if(i<10)i='0'+i;
			if(s<10)s='0'+s;
			if(ms<10)ms='0'+ms;
			minute.html(i);
			second.html(s);
			millisecond.html(ms);
		}, 41);
	}
});
</script>
<?php echo $shareScript;?>
</div>
</body>
</html>