<include file="Common:public_header" />
</head>
<body style="background:#fff !important;">
<!-- 内页顶部 -->
<include file="Common:top" />
<!-- 内页顶部 -->

    <input name="hidCodeID" type="hidden" id="hidCodeID" value="18101" />
    <input name="hidIsEnd" type="hidden" id="hidIsEnd" value="1" />

    <!-- 抢购记录 -->
	<div id="wrapper" style="bottom:0px;">
    <section id="buyRecordPage" class="goodsCon">
        
        <?php if (!empty($records)){ ?>
        <div id="divRecordList" class="recordCon z-minheight" style="display:block;">
           <?php foreach ($records as $key => $value){ ?>
 
			<ul>
				<li class="rBg">
				<a href="<?php echo U('User/userindex',array('uid' => $value['uid']));?>">
					<img id="imgUserPhoto" style="border-radius:50%;" class="lazy" data-original="<?php echo C('PIC_URL').get_user_key($value['uid'],'img');?>"   border="0"/>
					</a>
				</li>
				<li class="rInfo"><a href="<?php echo U('User/userindex',array('uid' => $value['uid']));?>"><?php echo get_user_name($value['userId']);?></a>
					<strong><?php if ($value['ip']){ ?>
							(<?php echo get_ip($value['id'],'ipcity');?> IP:<?php echo get_ip($value['id'],'ipmac');?>)
							<?php } ?></strong><br>
					<span>购买了<b class="orange"><?php echo $value['gonumber'];?></b>人次</span><em class="arial"><?php echo date("Y-m-d H:i:s",$value['time']);?></em>
				</li><i></i>
			</ul>
		  <?php } ?>
		  
		  
		</div>
        </div>
        <?php }else{ ?>
        
        <div id="divNone" class="haveNot z-minheight"><s></s><p>抱歉，该商品暂时没有购买记录！</p>
        <?php } ?>
    </section>

</div>

<div class="clear"></div>
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH;?>/assets/agile/css/ratchet/css/ratchet.min.css">
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH;?>/assets/agile/css/flat/iconline.css">
<script type="text/javascript">
lyzimg();
var Path = new Object();
Path.Skin="<?php echo MOBILE_TPL_PATH;?>";
Path.Webpath = "<?php echo WEB_URL;?>";
Path.M = "<?php echo MODULE_NAME;?>";
var Base = {head: document.getElementsByTagName("head")[0] || document.documentElement,Myload: function(B, A) {this.done = false;B.onload = B.onreadystatechange = function() {if (!this.done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {this.done = true;A();B.onload = B.onreadystatechange = null;if (this.head && B.parentNode) {this.head.removeChild(B)}}}},getScript: function(A, C) {var B = function() {};if (C != undefined) {B = C}var D = document.createElement("script");D.setAttribute("language", "javascript");D.setAttribute("type", "text/javascript");D.setAttribute("src", A);this.head.appendChild(D);this.Myload(D, B)},getStyle: function(A, B) {var B = function() {};if (callBack != undefined) {B = callBack}var C = document.createElement("link");C.setAttribute("type", "text/css");C.setAttribute("rel", "stylesheet");C.setAttribute("href", A);this.head.appendChild(C);this.Myload(C, B)}};
function GetVerNum() {var D = new Date();return D.getFullYear().toString().substring(2, 4) + '.' + (D.getMonth() + 1) + '.' + D.getDate() + '.' + D.getHours() + '.' + (D.getMinutes() < 10 ? '0': D.getMinutes().toString().substring(0, 1))}
Base.getScript('<?php echo MOBILE_TPL_PATH;?>/js/Bottom.js');
var shareData = {title: "<?php echo '(第'.$item['qishu'].'期)'._htmtocode($item['title']);?>",desc: "<?php echo _htmtocode($item['description']);?>",link: "<?php echo WEB_URL.U('Index/item',array('gid'=>$item['id']));?>",imgUrl: "<?php echo C('PIC_URL').$item['thumb'];?>"};
$(function(){
	$.jqScroll('#wrapper');
});
</script>
</body>
</html>
