!function(a,b,c,d){var e=a(b);a.fn.lazyload=function(f){function g(){var b=0;i.each(function(){var c=a(this);if(!j.skip_invisible||c.is(":visible"))if(a.abovethetop(this,j)||a.leftofbegin(this,j));else if(a.belowthefold(this,j)||a.rightoffold(this,j)){if(++b>j.failure_limit)return!1}else c.trigger("appear"),b=0})}var h,i=this,j={threshold:0,failure_limit:0,event:"scroll",effect:"show",container:b,data_attribute:"original",skip_invisible:!0,appear:null,load:null,placeholder:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"};return f&&(d!==f.failurelimit&&(f.failure_limit=f.failurelimit,delete f.failurelimit),d!==f.effectspeed&&(f.effect_speed=f.effectspeed,delete f.effectspeed),a.extend(j,f)),h=j.container===d||j.container===b?e:a(j.container),0===j.event.indexOf("scroll")&&h.bind(j.event,function(){return g()}),this.each(function(){var b=this,c=a(b);b.loaded=!1,(c.attr("src")===d||c.attr("src")===!1)&&c.is("img")&&c.attr("src",j.placeholder),c.one("appear",function(){if(!this.loaded){if(j.appear){var d=i.length;j.appear.call(b,d,j)}a("<img />").bind("load",function(){var d=c.attr("data-"+j.data_attribute);c.hide(),c.is("img")?c.attr("src",d):c.css("background-image","url('"+d+"')"),c[j.effect](j.effect_speed),b.loaded=!0;var e=a.grep(i,function(a){return!a.loaded});if(i=a(e),j.load){var f=i.length;j.load.call(b,f,j)}}).attr("src",c.attr("data-"+j.data_attribute))}}),0!==j.event.indexOf("scroll")&&c.bind(j.event,function(){b.loaded||c.trigger("appear")})}),e.bind("resize",function(){g()}),/(?:iphone|ipod|ipad).*os 5/gi.test(navigator.appVersion)&&e.bind("pageshow",function(b){b.originalEvent&&b.originalEvent.persisted&&i.each(function(){a(this).trigger("appear")})}),a(c).ready(function(){g()}),this},a.belowthefold=function(c,f){var g;return g=f.container===d||f.container===b?(b.innerHeight?b.innerHeight:e.height())+e.scrollTop():a(f.container).offset().top+a(f.container).height(),g<=a(c).offset().top-f.threshold},a.rightoffold=function(c,f){var g;return g=f.container===d||f.container===b?e.width()+e.scrollLeft():a(f.container).offset().left+a(f.container).width(),g<=a(c).offset().left-f.threshold},a.abovethetop=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollTop():a(f.container).offset().top,g>=a(c).offset().top+f.threshold+a(c).height()},a.leftofbegin=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollLeft():a(f.container).offset().left,g>=a(c).offset().left+f.threshold+a(c).width()},a.inviewport=function(b,c){return!(a.rightoffold(b,c)||a.leftofbegin(b,c)||a.belowthefold(b,c)||a.abovethetop(b,c))},a.extend(a.expr[":"],{"below-the-fold":function(b){return a.belowthefold(b,{threshold:0})},"above-the-top":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-screen":function(b){return a.rightoffold(b,{threshold:0})},"left-of-screen":function(b){return!a.rightoffold(b,{threshold:0})},"in-viewport":function(b){return a.inviewport(b,{threshold:0})},"above-the-fold":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-fold":function(b){return a.rightoffold(b,{threshold:0})},"left-of-fold":function(b){return!a.rightoffold(b,{threshold:0})}})}(jQuery,window,document);
function lyzimg(){
    $("img.lazy").lazyload({effect: "fadeIn"});
}

(function(){

	$('body').on('click','.login-close',function(){
		var _obj = $("#login").parent();
		var _mask = _obj.prev();
		_obj.remove();
		_mask.remove();
	});
	

	//右上角，弹出添加好友
	var flag = 1;
	$('#btn_add_friend').on('click',function(){
		if(flag == 1){
			flag = 2;
			$('.add_friend_box').fadeIn();
		}else if(flag == 2){
			flag = 1;
			$('.add_friend_box').fadeOut();
		}
	});
	$(window).on('touchstart',function(e){
		var target = $(e.target);
		if(target.closest('#btn_add_friend').length == 0 && target.closest('.add_friend_box').length == 0){
			flag = 1;
			$('.add_friend_box').fadeOut();
		}
	});
	$('.box_main span').click(function(){
		flag = 1;
		$('.add_friend_box').fadeOut();
		var type = $(this).attr('type');
		//添加好友
		if(type == 'addFriend'){
			var $popup = A.popup({
		   		html: $('#add_template_friend').html(),
		        css : {width:'auto'},
		        pos : 'center'
		    });
		    $popup.popup.find('button').on(A.options.clickEvent, function(){
		    	A.confirm('提示','确定添加吗？', function(){
		    		$popup.close();
					A.showToast('等待对方确认...');
		    	});
		    });
		}else if(type == 'createGroup'){
			var $popup = A.popup({
		   		html: $('#add_template_group').html(),
		        css : {width:'auto'},
		        pos : 'center'
		    });
		    $popup.popup.find('button').on(A.options.clickEvent, function(){
		    	A.confirm('提示','确定创建这个群吗？', function(){
		    		$popup.close();
					A.showToast('等待管理员审核...');
		    	});
		    });
		}
	});
	
	
})();

(function(a) {
    a.PageDialog = function(d, m) {
        var g = {
            W: 255,
            H: 45,
            obj: null,
            oL: 0,
            oT: 0,
            autoClose: true,
            autoTime: 1000,
            ready: function() {},
            submit: function() {}
        };
        var e = {
            obj: null,
            oL: 0,
            oT: 0,
            autoClose: true,
            autoTime: 1000,
            ready: function() {},
            submit: function() {}
        };
        m = m || e;
        a.extend(g, m);
        var c = g.autoClose;
        var j = function(q) {
            var n = q.get(0);
            n.addEventListener("touchstart", p, false);
            function p(s) {
                if (s.touches.length === 1) {
                    n.addEventListener("touchmove", o, false);
                    n.addEventListener("touchend", r, false);
                }
            }
            function o(s) {
                s.preventDefault();
            }
            function r(s) {
                n.removeEventListener("touchmove", o, false);
                n.removeEventListener("touchend", r, false);
            }
        };
        var l = a("#pageDialogBG");
        if (!c && l.length == 0) {
            l = a('<div id="pageDialogBG" class="pageDialogBG"></div>');
            l.appendTo("body");
            j(l);
        }
        var i = a("#pageDialog");
        if (i.length == 0) {
            i = a('<div id="pageDialog" class="pageDialog" />');
            i.appendTo("body");
            if (!c) {
                j(i);
            }
        }
        var b = a(window);
        if (g.obj != null) {
            if (g.obj.length < 1) {
                g.obj = null;
            }
        }
        i.css({
            width: g.W + "px",
            height: g.H + "px"
        });
        i.html(d);
        var k = function() {
            var n, p, q;
            if (g.obj != null) {
                var o = g.obj.offset();
                n = o.left + g.oL;
                p = o.top + g.obj.height() + g.oT;
                q = "absolute";
            } else {
                n = (b.width() - g.W) / 2;
                p = (b.height() - g.H) / 2;
                q = "fixed";
            }
            i.css({
                position: q,
                left: n,
                top: p
            });
        };
        k();
        b.resize(k);
        var h = function() {
            b.unbind("resize");
            if (c) {
                i.fadeOut("slow");
            } else {
                i.hide();
                l.hide();
            }
        };
        var f = function() {
            g.submit();
            h();
        };
        if (c) {
            i.fadeIn(500);
        } else {
            l.show();
            i.show();
        }
        i.ready = g.ready();
        if (c) {
            window.setTimeout(f, g.autoTime);
        }
        this.close = function() {
            f();
        };
        this.cancel = function() {
            h();
        };
    };
    a.PageDialog.ok = function(c, b) {
        a.PageDialog('<div class="Prompt">' + c + "</div>", {
            submit: (b === undefined ?
            function() {}: b)
        });
    };
    a.PageDialog.fail = function(e, d, f, b, c) {
        a.PageDialog('<div class="Prompt">' + e + "</div>", {
            obj: d,
            oT: f,
            oL: b,
            autoTime: 2000,
            submit: (c === undefined ?
            function() {}: c)
        });
    };
    
    a.PageDialog.confirm = function(g, c, b) {
        var e = null;
        var d = '<div class="clearfix m-round u-tipsEject"><div class="u-tips-txt">' + g + '</div><div class="u-Btn"><div class="u-Btn-li"><a href="javascript:;" id="btnMsgCancel" class="z-CloseBtn">取消</a></div><div class="u-Btn-li"><a id="btnMsgOK" href="javascript:;" class="z-DefineBtn">确定</a></div></div></div>';
        var f = function() {
            a("#btnMsgCancel").click(function() {
                e.cancel();
            });
            a("#btnMsgOK").click(function() {
                e.close();
            });
        };
        e = new a.PageDialog(d, {
            H: (b === undefined ? 126 : b),
            autoClose: false,
            ready: f,
            submit: c
        });
    };
})(jQuery);


/**
 * ajax登录用户
 */
$(function(){
	$('#login-form').submit(function(form){
		var account = $('#txtAccount');
		var password = $('#txtPassword');
		$(this).ajaxSubmit({
			type : 'POST',
			url : '?/mobile/ajax/userlogin',
			data : {
				'u' : 'xxx',
				'password' : base64encode(utf16to8(password.val()))
			},  //额外参数
			beforeSubmit : function(){
				if(account.val() == ''){
				    $.PageDialog.fail('手机号或邮箱不能为空');
					return false;
				}
				if(password.val() == ''){
					$.PageDialog.fail('登录密码不能为空');
					return false;
				}
			},
			success : function(responseText,status,xhr){
				var json = $.parseJSON(responseText);
				if(json['state'] == 1 && json['num'] == -2){
					$.PageDialog.fail('账号不存在');
					return false;
				}else if(json['state'] == 2){ //未认证，跳转认证
					$.PageDialog.fail('帐号未认证，跳转认证');
					return false;
				}else if(json['state'] == 1 && json['num'] == -1){
					$.PageDialog.fail('帐号或密码错误');
					return false;
				}else if(json['state'] == 0){
					$.PageDialog.ok('登录成功');
					setTimeout(function(){
					    window.location.href = '?/mobile/home';
					},2000);
				}
			}
		});
		return false;
	});

var base64encodechars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    var base64decodechars = new Array(
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
    -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
    -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1);
    function base64encode(str) {
        var out, i, len;
        var c1, c2, c3;
        len = str.length;
        i = 0;
        out = "";
        while (i < len) {
            c1 = str.charCodeAt(i++) & 0xff;
            if (i == len) {
                out += base64encodechars.charAt(c1 >> 2);
                out += base64encodechars.charAt((c1 & 0x3) << 4);
                out += "==";
                break;
            }
            c2 = str.charCodeAt(i++);
            if (i == len) {
                out += base64encodechars.charAt(c1 >> 2);
                out += base64encodechars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xf0) >> 4));
                out += base64encodechars.charAt((c2 & 0xf) << 2);
                out += "=";
                break;
            }
            c3 = str.charCodeAt(i++);
            out += base64encodechars.charAt(c1 >> 2);
            out += base64encodechars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xf0) >> 4));
            out += base64encodechars.charAt(((c2 & 0xf) << 2) | ((c3 & 0xc0) >> 6));
            out += base64encodechars.charAt(c3 & 0x3f);
        }
        return out;
    }
    function base64decode(str) {
        var c1, c2, c3, c4;
        var i, len, out;
        len = str.length;
        i = 0;
        out = "";
        while (i < len) {

            do {
                c1 = base64decodechars[str.charCodeAt(i++) & 0xff];
            } while (i < len && c1 == -1);
            if (c1 == -1)
                break;

            do {
                c2 = base64decodechars[str.charCodeAt(i++) & 0xff];
            } while (i < len && c2 == -1);
            if (c2 == -1)
                break;
            out += String.fromCharCode((c1 << 2) | ((c2 & 0x30) >> 4));

            do {
                c3 = str.charCodeAt(i++) & 0xff;
                if (c3 == 61)
                    return out;
                c3 = base64decodechars[c3];
            } while (i < len && c3 == -1);
            if (c3 == -1)
                break;
            out += String.fromCharCode(((c2 & 0xf) << 4) | ((c3 & 0x3c) >> 2));

            do {
                c4 = str.charCodeAt(i++) & 0xff;
                if (c4 == 61)
                    return out;
                c4 = base64decodechars[c4];
            } while (i < len && c4 == -1);
            if (c4 == -1)
                break;
            out += String.fromCharCode(((c3 & 0x03) << 6) | c4);
        }
        return out;
    }
    function utf16to8(str) {
        var out, i, len, c;
        out = "";
        len = str.length;
        for (i = 0; i < len; i++) {
            c = str.charCodeAt(i);
            if ((c >= 0x0001) && (c <= 0x007f)) {
                out += str.charAt(i);
            } else if (c > 0x07ff) {
                out += String.fromCharCode(0xe0 | ((c >> 12) & 0x0f));
                out += String.fromCharCode(0x80 | ((c >> 6) & 0x3f));
                out += String.fromCharCode(0x80 | ((c >> 0) & 0x3f));
            } else {
                out += String.fromCharCode(0xc0 | ((c >> 6) & 0x1f));

                out += String.fromCharCode(0x80 | ((c >> 0) & 0x3f));
            }
        }
        return out;
    }
    function utf8to16(str) {
        var out, i, len, c;
        var char2, char3;
        out = "";
        len = str.length;
        i = 0;
        while (i < len) {
            c = str.charCodeAt(i++);
            switch (c >> 4) {
                case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                    // 0xxxxxxx
                    out += str.charAt(i - 1);
                    break;
                case 12: case 13:
                    // 110x xxxx   10xx xxxx
                    char2 = str.charCodeAt(i++);
                    out += String.fromCharCode(((c & 0x1f) << 6) | (char2 & 0x3f));
                    break;
                case 14:
                    // 1110 xxxx  10xx xxxx  10xx xxxx
                    char2 = str.charCodeAt(i++);
                    char3 = str.charCodeAt(i++);
                    out += String.fromCharCode(((c & 0x0f) << 12) |
                       ((char2 & 0x3f) << 6) |
                       ((char3 & 0x3f) << 0));
                    break;
            }
        }
        return out;
    }
    //base64 加密base64encode(utf16to8(value));
    //base64 解密utf8to16(base64decode(value));
	
	//飞入购物车动画
	function anim(_this){
		var img = _this.parents('li').find('img');
		var flyElm = img.clone().css('opacity', 0.85);
		$('body').append(flyElm);
		flyElm.css({
			'z-index': 9000,
			'display': 'block',
			'position': 'absolute',
			'top': img.offset().top +'px',
			'left': img.offset().left +'px',
			'width': img.width() +'px',
			'height': img.height() +'px'
		});
		flyElm.animate({
			top: $('.head-shopcart').offset().top,
			left: $('.head-shopcart').offset().left,
			width: 20,
			height: 32
		}, 'slow', function() {
			flyElm.remove();
		});
	}
	

    $("body").on("click",'.addFreeCart',function(){

        var goods_id = $(this).attr('item');
        if(f_flag == false){
            return;
        }
        _this = $(this);
        $.ajax({
            type : 'POST',
            url : '?/mobile/ajax/addShopFreeCart/'+goods_id+'/'+1,
            beforeSend : function(){
                if (f_flag == true) {
                    //anim(_this);
                    f_flag = false;
                }
            },
            success : function(res){

                // console.log(res);
                var res = $.parseJSON(res);
                f_flag = true;
                if (res['code'] == 0) {
                    if(res['num'] > 0){
                        window.location.href='?/mobile/cart/freecartlist/';
                    }
                }
            }
        });





    });


	$.extend({'jqScroll':function(domId,scrollbars){
		if(scrollbars == undefined){
			var ON_scrollbars = 'custom';
		}else{
			var ON_scrollbars = scrollbars;
		}
	var myScroll;
	var loaded = function(e){
		myScroll = new IScroll(domId,{
			mouseWheel : true,
			usetransform : false,
			click : false,
			scrollbars: ON_scrollbars,
			//scrollbars : false,
			bounce : true,
			interactiveScrollbars: true,
			shrinkScrollbars: 'scale',
			fadeScrollbars: true,
			isBadAndroid : false,	//防止安卓页面抖动
			preventDefaultException: { tagName: /^(INPUT|TEXTAREA|BUTTON|SELECT|A|IMG|SPAN|S|DIV|UL|LI|DL|DT|DD)$/ },
			onscrollmove : function(){
				alert(domId);
				domId.trigger('scroll');
			},
		});
	};
	document.addEventListener('touchmove',function(e){
		e.preventDefault();
	});
	//document.addEventListener('DOMContentLoaded',loaded,false);
	loaded();
	//开始滚动时执行，
//	myScroll.on('scrollStart',function(){
//		alert('开始滚动时执行');
//	});

//	myScroll.on('scrollCancel',function(){
//		alert('滚动被取消执行');
//	});
//	myScroll.on('beforeScrollStart',function(){
//		alert('开始滚动之前执行');
//	});

	myScroll.on('scrollEnd',function(){
		$(domId).trigger('scroll');
	});
	return myScroll;
 }});
	
	

});

//打窗口
function openWin(url,type){
	var target = '_self';
	if(type != ''){
		target = type;
	}
	window.open(url,target);
}

//设置滚动函数，jquery外面调用
function myScroll(domId){
	var myScroll;
	var loaded = function(e){
		myScroll = new IScroll(domId,{
			mouseWheel : true,
			click : false,
			//scrollbars : true,
			interactiveScrollbars: true,
			shrinkScrollbars: 'scale',
			scrollbars: 'custom',
			fadeScrollbars: true,
			isBadAndroid : false,	//防止安卓页面抖动
			preventDefaultException: { tagName: /^(INPUT|TEXTAREA|BUTTON|SELECT|A|IMG|S)$/ },
		});
	};
	document.addEventListener('touchmove',function(e){
		e.preventDefault();
	});
	//document.addEventListener('DOMContentLoaded',loaded,false);
	loaded();
	//开始滚动时执行，
//	myScroll.on('scrollStart',function(){
//		alert('开始滚动时执行');
//	});

//	myScroll.on('scrollCancel',function(){
//		alert('滚动被取消执行');
//	});
//	myScroll.on('scroll',function(){
//		alert('滚动中执行');
//	});
//	myScroll.on('beforeScrollStart',function(){
//		alert('开始滚动之前执行');
//	});

//	myScroll.on('scrollEnd',function(){
//		alert('滚动结束时执行');
//	});
	return myScroll;
}

function getCartCount(){
	$.ajax({
		type : 'GET',
		url : '?/mobile/ajax/cartnum',
		success : function(data){
			var data = $.parseJSON(data);
			if(data['code'] == 0 && data['num'] > 0){
				$('.footer_goods_num').text(data['num']);
				$('.footer_goods_num').show();
			}
		}
	});
	setTimeout(getCartCount,3000);
}
//getCartCount();

