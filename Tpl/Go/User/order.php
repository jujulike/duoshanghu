<include file="Common:header" />
	</head>
	<body>
		<div id="section_container">
			<section id="index_section" data-role="section" class="active">
				
				<header class="bar bar-nav" id="header">
				  	<a class="icon icon-left-nav pull-left"  href="javascript:;"  onclick="history.go(-1)"></a>
				  	<h1 class="title"><?php echo $title;?></h1>
				</header>
				<header class="bar bar-nav box_shadow head_a" style="margin-top:43px;padding-left:5px;padding-right:5px;">
					<a href="<?php echo U('User/userbuylist');?>" style="width:50%;" class="jx">购买记录</a>
					<a href="<?php echo U('User/order');?>" style="width:50%;" class="lastnew selected_bottom">中奖记录</a>
				</header>							
<article data-role="article" id="flat_article" data-scroll="verticle" class="active" style="top:65px;bottom:50px;">

	<div class="scroller create_goods">
		<?php foreach ($orderlist as $key => $value){ ?>
	    <ul class="listitem group_listitem">
	        <li style="border-bottom:1px solid #e4e4e4;">
	        <a href="<?php echo U('ubuydetail',array('gid' => $value['id']));?>">
	            <img data-original="<?php echo C('PIC_URL').$value['thumb'];?>" width="90" height="90" class="img lazy"/>
	         </a>
	        	<div class="text">
	              	<p><a href="<?php echo U('ubuydetail',array('gid' => $value['id']));?>"><?php echo $value['shopname'];?></a></p>
	                <p><span>购物期数：第<?php echo $value['shopqishu'];?>期</span></p>
	            	<p><span>已参与：<?php echo $value['gonumber'];?>次</span><a href="<?php echo U('ubuydetail',array('gid' => $value['id']));?>" class="common-btn view_detail">查看详情</a></p>
	            	<p><span>幸运抢购码：<?php echo $value['huode'];?></span></p>
	            	<p><span>揭晓时间：<?php echo $value['q_end_time'];?></span></p>
				</div>
	        </li>
	    </ul>
	    <?php } ?>
	</div>


</article>
<include file="Common:footer" />
<script type="text/javascript">
lyzimg();
$(function(){
	var page = 1;
	$('.loading-more').click(function(){
		var _this = $(this);
		_this.hide();
		if(_this.attr('count') == 'false'){
			alert('别点了，没数据了...');
			return;
		}
		page++;
		$.ajax({
			type:'get',
			url:'/index.php?m=Go&c=User&a=userbuylist&page='+page,
			success:function(data){
				_this.show();
				if(data['count'] == 0){
					_this.attr('count','false');
					page = data['pagetotal'];
					return;
				}
				var html = '';
				for(var i in data['data']){
				    html +='<ul class="listitem group_listitem"><li>';
				    html +='<a href="/index.php?m=Go&c=User&a=ubuydetail&gid='+data['data'][i]['id']+'">';
				    html +='<img data-original="<?php echo C('PIC_URL');?>'+data['data'][i]['thumb']+'" width="25%" class="img lazyimg'+page+'"/>';
				    html +='</a><div class="text">';
				    html +='<p><a href="/index.php?m=Go&c=User&a=ubuydetail&gid='+data['data'][i]['id']+'">'+data['data'][i]['shopname']+'</a></p>';
				    html +='<p><span>购物期数：第'+data['data'][i]['shopqishu']+'期</span></p>';
				    html +='<p><span>已参与：'+data['data'][i]['gonumber']+'次</span><a href="/index.php?m=Go&c=User&a=ubuydetail&gid='+data['data'][i]['id']+'" class="common-btn view_detail">查看详情</a></p>';
					html +='</div></li></ul>';
				}
				_this.before(html);
				setTimeout(function(){
					$("img.lazyimg"+page).lazyload({effect: "fadeIn"});
					$.jqScroll('#flat_article');
				},150);
			}
		});
	});
});
</script>
</body>
</html>