<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>购物车</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0"/>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH;?>/css/top.css">
<link href="<?php echo MOBILE_TPL_PATH;?>/css/comm.css" rel="stylesheet" type="text/css" />
<script src="<?php echo MOBILE_TPL_PATH;?>/js/jquery.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo MOBILE_TPL_PATH;?>/js/common.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo MOBILE_TPL_PATH;?>/js/iscroll5.js" language="javascript" type="text/javascript"></script>
<link href="<?php echo MOBILE_TPL_PATH;?>/css/cartList.css" rel="stylesheet" type="text/css" />
<script id="pageJS" data="<?php echo MOBILE_TPL_PATH;?>/js/Cartindex.js" language="javascript" type="text/javascript"></script>
</head>
<body>
<div class="h5-1yyg-v1" id="loadingPicBlock">

<header class="bar bar-nav" id="header">
<a class="icon icon-left-nav pull-left" href="javascript:;"  onclick="history.go(-1)"></a>
<h1 class="title">购物车</h1> 
</header>

    <input name="hidLogined" type="hidden" id="hidLogined" value="1" />
	<div id="wrapper">
    <section class="clearfix g-Cart">
		<?php if ($shop){ ?>	    
	        <article class="clearfix m-round g-Cart-list">
	            <ul id="cartBody">
				<?php $buyshopmoney=0;
				
					foreach ($shoplist as $key => $val){
						$num = count($shoplist);
						$buyshopmoney+=$Mcartlist[$val['id']]['num']*$Mcartlist[$val['id']]['money'];
						
				?>
					<li>
						<a class="fl u-Cart-img" href="<?php echo U('Index/item',array('gid' => $val['id']));?>">
							<img data-original="<?php echo C('PIC_URL').$val['thumb'];?>" border="0" class="lazy" alt="<?php echo $val['title'];?>"/>
						</a>
						<div class="u-Cart-r">
							<p class="z-Cart-tt"><a href="<?php echo U('Index/item',array('gid' => $val['id']));?>" class="gray6">(第<?php echo $val['qishu'];?>期)<?php echo $val['title'];?></a></p>
							<ins class="z-promo gray9">剩余<em class="arial"><?php echo $val['zongrenshu']-$val['canyurenshu'];?></em>人次</ins>
							<p class="gray9">总共抢购：<em class="arial"><?php echo $Mcartlist[$val['id']]['num'];?></em>人次/<em class="orange arial">￥<?php echo $Mcartlist[$val['id']]['money']*$Mcartlist[$val['id']]['num'];?>.00</em></p>
							<p class="f-Cart-Other">
								<a href="javascript:;" class="fr z-del delgoods" cid="<?php echo $val['id'];?>"></a>
								<a href="javascript:;" class="fl z-jian <?php if ($Mcartlist[$val['id']]['num']==1){?>z-jiandis<?php }?>">-</a>
								<input id="txtNum<?php echo $val['id'];?>" name="num" data-money="<?php echo $Mcartlist[$val['id']]['money'];?>" type="text" maxlength="7" value="<?php echo $Mcartlist[$val['id']]['num'];?>" class="fl z-amount" style="height:28px;padding:0;" />
								<a href="javascript:;" class="fl z-jia <?php if($Mcartlist[$val['id']]['num']==$val['zongrenshu']){ ?>z-jiadis<?php }?>">+</a>
								<input type="hidden" value="<?php echo $Mcartlist[$val['id']]['num'];?>" />
								<input type="hidden" value="<?php echo $val['zongrenshu']-$val['canyurenshu'];?>" />
							</p>
						</div>
					</li>
			<?php } ?>
	            </ul>
	        </article>

	    <div id="divBtmMoney" class="g-Total-bt"><p>总共抢购
			<span class="orange arial z-user"><?php echo $num;?></span>个商品  合计金额：
			<span class="orange arial"><?php echo $buyshopmoney;?>.00</span> 元</p>
			<a href="javascript:;" class="orgBtn common-btn" style="width:75%;margin:0 auto;">结 算</a>
		</div>
	<?php } ?>
	    <div id="divNone" class="haveNot z-minheight" style="display:none"><s></s><p>抱歉，您的购物车没有商品记录！</p>
		</div>
		<div class="block30"></div>
    </section>
</div>
</div>
<!-- 底部开始 -->
<include file="Common:footer2"/>
<script type="text/javascript">
$(function(){
	$.jqScroll('#wrapper');
	$('.delgoods').click(function(){
		var obj = $(this);
		var goods_id = obj.attr('cid');
		layer.open({
	    content: '确认删除商品吗？',
	    btn: ['确认', '取消'],
	    shadeClose: false,
	    yes: function(){
	        layer.open({content: '你点了确认', time: 1});
			$.ajax({
				type : 'POST',
				url : '?m=Go&c=Ajax&a=delCartItem&gid='+goods_id,
				dataType : 'json',
				success : function(data){
					if(data['code'] == 1){
						layer.open({content: '删除失败', time: 1});
					}else{
						obj.parents('li').remove();
						window.location.reload();
					}
				},
			});
	    }, no: function(){
	        
	    }
	}); 
	});
});
</script>
</body>
</html>