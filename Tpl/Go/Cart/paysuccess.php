<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>支付成功</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
    <meta content="telephone=no" name="format-detection" />
    <meta name="CMBNETPAYMENT" content="China Merchants Bank">
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH;?>/css/top.css">
<link href="<?php echo MOBILE_TPL_PATH;?>/css/comm.css" rel="stylesheet" type="text/css" />
<link href="<?php echo MOBILE_TPL_PATH;?>/css/cartList.css" rel="stylesheet" type="text/css" />
<script src="<?php echo MOBILE_TPL_PATH;?>/js/jquery.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo MOBILE_TPL_PATH;?>/js/common.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo MOBILE_TPL_PATH;?>/js/iscroll5.js" language="javascript" type="text/javascript"></script>
</head>
<body>
    <div class="h5-1yyg-v1">
<style>
.z-pay-tips s{
	margin:0 auto;
	display:block;
	float:none;
}
.g-pay-success{
	background:inherit;
}
</style>
<!-- 栏目页面顶部 -->

<header class="bar bar-nav" id="header">
<a class="icon icon-left-nav pull-left" href="javascript:;"  onclick="history.go(-1)"></a>
<h1 class="title">支付成功</h1> 
</header>

        <input name="hidShopID" type="hidden" id="hidShopID" value="131118151938166319">
        <div id="wrapper">
			<section id="shopResultBox" class="clearfix g-pay-success">
			<div class="g-pay-auto">
			<div class="z-pay-tips"><s></s><b><em class="gray6">恭喜您,支付成功，请等待系统为您揭晓！</em></b></div>
			</div>
			<div class="u-Btn"><div class="u-Btn-li"><a href="<?php echo U('User/userbuylist');?>" class="z-CloseBtn">查看抢购记录</a></div><div class="u-Btn-li"><a href="<?php echo U('Index/index');?>" class="z-DefineBtn" style="background: #ff1b1b;">继续购物</a></div></div>
			</section>
		</div>
    </div>

<include file="Common:footer2"/>
</body>
</html>
