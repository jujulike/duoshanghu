/**
 * 首页JS
 */
//精品选购
function isSale(_this){
	var input='#'+$(_this).attr('data-input');
	var id='#'+$(_this).attr('data-id');
	var type=$(_this).attr('data-type');
	var url=$(_this).attr('data-url');
	var page=$(input).val();
	   $.ajax({
           type: "POST",
           url: url,
           data: {page:page,type:type},
           dataType: "json",
           success: function(data){
           	if(data!=null){
        	   if(data.length==3){
        		   page++;
        	       $(input).val(page);
        	       var html = ''; 
                   $.each(data, function(commentIndex, comment){
                		 html+='<a href="/Wx/Index/goodsDetail/r/index/id/'+comment['goodsId']+'">';
                    	 html+='	<div class="_imgs"><img src="/'+comment['goodsThums']+'" /></div>';
                    	 html+='<div class="txt-l goodstitle">'+comment["goodsName"]+'.</div>';
                    	 html+='<div class="txt-l goodstitle col-red">￥'+comment["shopPrice"]+'</div></a>';
                   });
                   $(id).html(html);
        	   }else{
        		   $(input).val(0);
        	   }
			}else{
				$(input).val(0);
			}
           
            }
       });
}

//超市换一组
function switchMoreSupermarket(_this){
	var input='#'+$(_this).attr('data-input');
	var id='#'+$(_this).attr('data-id');
	var type=$(_this).attr('data-type');
	var url=$(_this).attr('data-url');
	var page=$(input).val();
	   $.ajax({
           type: "POST",
           url: url,
           data: {page:page},
           dataType: "json",
           success: function(data){
        	   console.log(data);
			   if(data!=null) {
				   if (data.length == 3) {
					   page++;
					   $(input).val(page);
					   var html = '';
					   $.each(data, function (commentIndex, comment) {
						   html += '<div class="_img_jin _margin_left10 pos-r" onclick="javascript:window.location.href=\'' + comment["url"] + '\'"><div class="pos-a">';
						   html += '<div class="_imgWidth "><img src="/' + comment["goodsThums"] + '" /></div>';
						   html += '<div class="_jin_text font90">' + comment["goodsName"] + '</div>';
						   html += '<div class="_jin_money font90">￥' + comment["shopPrice"] + '</div></div></div>';
					   });
					   $(id).html(html);
				   } else {
					   $(input).val(0);
				   }
			   }else{
				   $(input).val(0);
			   }
           
            }
       });
}





//秒杀换一组
function secKill(_this){
	var url=$(_this).attr('data-url');
	var page=$('#secKill_page').val();
	   $.ajax({
           type: "POST",
           url: url,
           data: {page:page},
           dataType: "json",
           success: function(data){
        	   if(data!=null){
        		   if(data.length==3){
        			   page++;
        		   }
        	       $('#secKill_page').val(page);
        	       var html = ''; 
                   $.each(data, function(commentIndex, comment){
                	 html+='<a href="Index/goodsDetail/id/'+comment['goodsId']+'/secKill/1">';
                	 html+='	<div class="_imgs"><img src="/'+comment['goodsThums']+'" /></div>';
                	 html+='<div class="txt-l goodstitle">'+comment["goodsName"]+'.</div>';
                	 html+='<div class="txt-l goodstitle col-red">￥'+comment["shopPrice"]+'</div></a>';
                   });
                   $('#secKill_html').html(html);
        	   }else{
        		   $('#secKill_page').val(0);
        	   }
            }
       });
}
//猜你喜欢
function guess(_this){
	var url=$(_this).attr('data-url');
	$.ajax({
		type: "POST",
		url: url,
		data: {},
		dataType: "json",
		success: function(data){
			if(data!=null){
				var html = ''; 
				$.each(data, function(commentIndex, comment){
					html+='<a href="/Wx/Index/goodsDetail/r/my/id/'+comment['goodsId']+'">';
					html+='	<div class="_imgs"><img src="/'+comment['goodsThums']+'" /></div>';
					html+='<div class="txt-l goodstitle">'+comment["goodsName"]+'.</div>';
					html+='<div class="txt-l goodstitle col-red">￥'+comment["shopPrice"]+'</div></a>';
				});
				$('#guess_html').html(html);
			}
		}
	});
}
