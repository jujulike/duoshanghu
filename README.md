# 嘟嘟多商户

#### 介绍
嘟嘟多商户项目，是有电商，入驻商，代理商，app电商前端等功能一整套电商平台，供大家学习用。请大家加入QQ群交流：881437201



#### 后端管理结构目录介绍

Application 应用目录
Cjson         app端Api接口目录
Common     公共目录
Conf           配置目录
Lang           核心语言包目录
Library        框架类库目录
Mode          框架应用模式目录
Public         资源目录
Runtime      运行时目录
Tpl             前端模板目录
Upload       文件上传目录
db           为数据库文件
.htaccess    目录级别的修改配置的方式
admin.php  总管理后端入口
index.php   站点首页
shop.php    商家入口



#### 技术

后端技术
1. 基于Thinkphp框架
2. Json方式提供Api
3. kindeditor
4. bootstrap
5. layer


PC前端技术

1. bootstrap
2. H5

手机前端技术

1. bootstrap
2. H5


#### 总后端功能

1. 商品管理
2. 订单管理
3. 一元购
4. 提现管理
5. 分销管理
6. 店铺管理
7. 会员管理
8. 积分商城管理
9. 促销管理
10. 优惠活动管理
11. 地区管理
12. 论坛管理
13. 圈子管理
14. 文章管理
15. 统计管理
16. 系统管理
17. 商城设置

#### 商家后端功能

1. 订单管理
2. 商品管理
3. 网店设置
4. 优惠活动



#### 系统安装
apache 2.4.9（nginx也可用,版本1.1以上）
mysql  5.5 以上
php    5.6
代码中设置到的***.com域名，替换成自己的域名
设置到微信登录的。 替换自己的微信参数
超管后台  http://*.*.com/admin  帐号 admin   初始密码 123456
手机端访问地址http://*.*.com/wx
pc端端访问地址http://*.*.com
以上的[*.*.com]为实际的域名

#### 系统后台截图

![输入图片说明](https://images.gitee.com/uploads/images/2019/0415/160737_9268c691_448924.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0415/160748_be4c53d8_448924.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0415/160802_090860ae_448924.png "3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0415/160812_7ad89754_448924.png "4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0415/160822_df361465_448924.png "5.png")





#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)