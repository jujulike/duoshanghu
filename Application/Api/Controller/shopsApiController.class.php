<?php
namespace Api\Controller;

use Think\Controller;
class  shopsApiController extends Controller {

    public function proportion($shopId,$ordersId){
        $res = false;
        $proportion = M('shops')->where(array('shopId'=>$shopId))->getField('proportion');

        $orderInfo = M('orders')->field('userId,needPay')->where(array('orderId'=>$ordersId,'isPay'=>1,'payType'=>array('neq','0')))->find();
        $pushAdmin = $proportion*$orderInfo['needPay'];
        $pushShop =  $orderInfo['needPay']-$pushAdmin;

        $data['orderId'] = $ordersId;
        $data['proportion'] = $proportion;
        $data['userId'] = $orderInfo['userId'];
        $data['money']   = $orderInfo['needPay'];
        $data['time']=date('Y-m-d H:i:s',time());
        $data['shopId']=$shopId;
        $data['pushShop']=$pushShop;
        $data['pushAdmin']=$pushAdmin;

        $log = M('biz_income')->add($data);
        $s=  M('shops')->where(array('shopId'=>$shopId))->setInc('bizMoney',$pushShop);
        if($log&&$s){
            $res = true;
        }

        return $res;
    }
	
}