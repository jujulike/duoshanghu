<?php 
namespace Go\Util;
use Think\Model;
class tocode {

	private $go_list;
	public $go_code;
	public $go_content;
	public $cyrs;
	public $shop;
	public $count_time='';
	public $shishi_cai; //时时彩数据
	
	public function __construct() {
		$this->db = new Model();
	}	

	public function config($shop=null,$type=null){
		$this->shop = $shop;		
	}
	
	public function get_run_tocode(){
		
	}
	
	public function returns(){
		
	}
	
	public function run_tocode(&$time='',$num=100,$cyrs='233'){
		if(empty($time))return false;
		if(empty($num))return false;
		if(empty($cyrs))return false;
		$this->times = $time;
		$this->num = $num;
		$this->cyrs = $cyrs;
		$this->get_code_user_html();
		$this->get_user_go_code();
	}
	
	//组团计算商品+时时彩
	public function team_run_tocode(&$time='',$num=100,$cyrs='233'){
		if(empty($time))return false;
		if(empty($num))return false;
		if(empty($cyrs))return false;
		$this->times = $time;
		$this->num = $num;
		$this->cyrs = $cyrs;
		$this->team_get_code_user_html();
		//$this->team_get_user_go_code();
	}
	private function team_get_code_user_html(){
		$time = $this->times;
		$num  = $this->num;
		$this->go_list = $this->db->GetList("select * from `@#_member_team_record` where `time` < '$time' order by `id` DESC limit 0,$num");
		if($this->go_list  && count($this->go_list) >= $this->num){
			$this->team_get_code_dabai();
		}else{
			$this->team_get_code_yibai();
		}
	}
	private function team_get_user_go_code(){
		if(file_exists(G_SYSTEM.'modules/goodspecify/lib/'.'itocode.class.php')){
			$itocode = System::load_app_class("itocode","goodspecify");
			$itocode->team_go_itocode($this->shop,$this->go_code,$this->go_content,$this->count_time);
		}
		$this->team_get_code_user_html();
	}
	//抢购小于 100人的计算方式
	private function team_get_code_yibai(){
		$time = $this->times;
		$cyrs = $this->cyrs;
		$h=abs(date("H",$time));
		$i=date("i",$time);
		$s=date("s",$time);
		$w=substr($time,11,3);
		$num= $h.$i.$s.$w;
		if(!$cyrs){
			$cyrs=1;
		}
		//$this->go_code = 10000001+fmod($num*100,$cyrs);
		
		$total_h = $num*100 + (int)$this->shishi_cai['opencode'];
		$this->go_code = 10000001+fmod($total_h,$cyrs);
		
		$this->go_content = false;
	}
	//002953485
	private function team_get_code_dabai(){
		$go_list = $this->go_list;
		$html=array();
	
		//统计100条下单记录的时间
		$count_time = 0;
		foreach($go_list as $key=>$v){
			$html[$key]['time'] = $v['time'];
			$html[$key]['username'] = $v['username'];
			$html[$key]['uid'] = $v['uid'];
			$html[$key]['shopid'] = $v['shopid'];
			$html[$key]['shopname'] = $v['shopname'];
			$html[$key]['shopqishu'] = $v['shopqishu'];
			$html[$key]['gonumber'] = $v['gonumber'];
			$h=abs(date("H",$v['time']));
			$i=date("i",$v['time']);
			$s=date("s",$v['time']);
			list($time,$ms) = explode(".",$v['time']);
			$time = $h.$i.$s.$ms;
			$html[$key]['time_add'] = $time;
			$count_time += $time;
		}
	
		$this->go_content = serialize($html);
		$this->count_time=$count_time;
		
		$total_h = $this->count_time + (int)$this->shishi_cai['opencode'];
		
		$this->go_code = 10000001+fmod($total_h,$this->cyrs);
	}
	
	
	//num 50人排名
	public function run_game_tocode(&$time='',$num=50,$cyrs='233',$goods_info){
		if(empty($time))return false;
		if(empty($num))return false;
		if(empty($cyrs))return false;
		$this->times = $time;
		$this->num = $num;
		$this->cyrs = $cyrs;
		$time = $this->times;
		$num  = $this->num;
		$this->goods_info = $goods_info;
		$this->go_list = $this->db->GetList("select * from `@#_member_go_record` where `time` < '$time' order by `id` DESC limit 0,$num");
		$this->get_code_game();
	}
	
	//游戏50人的中奖方法
	private function get_code_game(){
		$cyrs = $this->cyrs;
		$num = $this->num;
		
		if($this->goods_info['game_id'] == 1){  //拼图游戏
			$sql = "select * from tp_qy_game_user_score order by score desc,id desc limit 0,{$num}";
		}elseif ($this->goods_info['game_id'] == 2){ //对对碰
			$sql = "select * from tp_game order by score desc,id desc limit 0,{$num}";
		}
		$list = $this->db->getList($sql);
		$_mst = date('ms',time());
		$count_time = 0;
		$count_score = 0;
		foreach ($list as $key => $value){
			$h = abs(date('H',$value['addtime']));
			$i = date('i',$value['addtime']);
			$s = date('s',$value['addtime']);
			list($time,$ms) = explode('.', $value['addtime']);
			$xtime = $h.$i.$s.$ms;
			$count_time += $xtime.$_mst;
			if($this->goods_info['game_id'] == 1){ //拼图游戏
				$count_score += $value['zongms'];
			}elseif($this->goods_info['game_id'] == 2){ //对对碰
				$count_score += $value['score'];
			}
		}
		$count_time*$count_score;
		//人数直接影响中奖概率
		$this->go_code = 10000001 + fmod(floatval($count_time/100)*$count_score,$cyrs);
		$this->go_content = false;
	}
	
	private function get_user_go_code(){
		/**
		 * if(file_exists(G_SYSTEM.'modules/goodspecify/lib/'.'itocode.class.php')):
		$itocode = System::load_app_class("itocode","goodspecify");
		$itocode->go_itocode($this->shop,$this->go_code,$this->go_content,$this->count_time);
		endif;
		 */
		$this->get_code_user_html();
	}
	
	private function get_code_user_html(){
		$time = $this->times;
		$num  = $this->num;
		$this->go_list = $this->db->query("select * from `__PREFIX__member_go_record` where `time` < '$time' order by `id` DESC limit 0,$num");
		if($this->go_list  && count($this->go_list) >= $this->num){
			$this->get_code_dabai();
		}else{
			//可以设置购买数量不够，不计算以下开奖码，不让别人中奖
			$this->get_code_yibai();
		}
	}
		
	//抢购小于 100人的计算方式
	private function get_code_yibai(){
		$time = $this->times;
		$cyrs = $this->cyrs;
		$h=abs(date("H",$time));
		$i=date("i",$time);
		$s=date("s",$time);
		$w=substr($time,11,3);
		$num= $h.$i.$s.$w;
		if(!$cyrs){
			$cyrs=1;
		}		
		$total_h = $num*100 + (int)$this->shishi_cai['opencode'];
		
		//$this->go_code = 10000001+fmod($num*100,$cyrs);
		$this->go_code = 10000001+fmod($total_h,$cyrs);
		$this->go_content = false;
	}
	
	//002953485
	private function get_code_dabai(){
		$go_list = $this->go_list;
		$html=array();
		
		//统计100条下单记录的时间
		$count_time = 0;
		foreach($go_list as $key=>$v){
			$html[$key]['time'] = $v['time'];	
			$html[$key]['username'] = $v['username'];	
			$html[$key]['uid'] = $v['uid'];
			$html[$key]['shopid'] = $v['shopid'];	
			$html[$key]['shopname'] = $v['shopname'];	
			$html[$key]['shopqishu'] = $v['shopqishu'];
			$html[$key]['gonumber'] = $v['gonumber'];			
			$h=abs(date("H",$v['time']));
			$i=date("i",$v['time']);
			$s=date("s",$v['time']);	
			list($time,$ms) = explode(".",$v['time']);
			$time = $h.$i.$s.$ms;
			$html[$key]['time_add'] = $time;
			$count_time += $time;
		}
		
		$this->go_content = serialize($html);
		
		$this->count_time = $count_time;
		
		$total_h = $this->count_time + (int)$this->shishi_cai['opencode'];
		
		$this->go_code = 10000001+fmod($total_h,$this->cyrs);
	}
	
	
	
	
			
}