<?php
namespace Go\Controller;
class UserController extends BaseController {
	
	protected function _initialize(){
		parent::_initialize();
	}
	
	private function isLogin(){
		if (!$this->userinfo){
			header("location: ".U('Wx/Login/login'));
			exit;
		}
	}
	
	public function order(){
		$this->isLogin();
		$model = new \Think\Model();
		$member=$this->userinfo;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$order=$model->query("select * from `__PREFIX__member_go_record` a left join `__PREFIX__shoplist` b on a.shopid=b.id where b.q_uid='$member[uid]' and b.q_showtime='N'" );
		$pageSize = 20;
		$pageTotal = ceil(count($order)/$pageSize);
		if ($page >= $pageTotal){
			$page = $pageTotal;
		}else {
			if (!$page){
				$page = 1;
			}
		}
		$limit = ($page-1)*$pageSize.','.$pageSize;
		$orderlist['listItems']=$model->query("select a.*,b.title,b.q_end_time as end_time,b.thumb from `__PREFIX__member_go_record` a left join `__PREFIX__shoplist` b on a.shopid=b.id where b.q_uid='$member[uid]' and a.huode!=0  and b.q_showtime='N' order by a.time desc limit $limit");
		if(empty($orderlist['listItems'])){
			$orderlist['code']=1;
		}else{
			foreach($orderlist['listItems'] as $key=>$val){
				$orderlist['listItems'][$key]['q_end_time']=microt($val['end_time']);
			}
			$orderlist['code']=0;
		}
		$orderlist['count']=count($order);
		$this->title = '中奖记录';
		if (IS_AJAX){
			$this->ajaxReturn(array('status' => 1,'data' => $orderlist));
		}else{
			$this->orderlist = $orderlist['listItems'];
		}
		$this->assign('re_active','active');
		$this->display();
	}

	//获得的商品
	public function mygoods(){
		$this->isLogin();
		$this->title = '中奖记录';
		if (IS_AJAX){
			$FIdx=intval($_GET['fx']);
			$EIdx=10;
			$member=$this->userinfo;
			$model = new \Think\Model();
			$order=$model->query("select * from `__PREFIX__member_go_record` a left join `__PREFIX__shoplist` b on a.shopid=b.id where b.q_uid='$member[uid]' and b.q_showtime='N'" );
			$orderlist['listItems']=$model->query("select * from `__PREFIX__member_go_record` a left join `__PREFIX__shoplist` b on a.shopid=b.id where b.q_uid='$member[uid]' and a.huode!=0  and b.q_showtime='N' order by a.time desc limit $FIdx,$EIdx " );
			if(empty($orderlist['listItems'])){
				$orderlist['code']=1;
			}else{
				foreach($orderlist['listItems'] as $key=>$val){
					unset($orderlist['listItems'][$key]['goucode']);
					unset($orderlist['listItems'][$key]['picarr']);
					$orderlist['listItems'][$key]['q_end_time']=microt($val['q_end_time']);
				}
				$orderlist['code']=0;
			}
			$orderlist['count']=count($order);
			echo json_encode($orderlist);
			exit;
		}
		$this->onscroll = true;
		$this->assign('re_active','active');
		$this->display();	
	}

	public function userindex(){
		$this->title = '个人主页';
		$uid = isset($_GET['uid'])?intval($_GET['uid']):0;
		if (!$uid) $this->error('用户不存在');
		$this->userinfo_x = M('Users')->where(array('userId' => $uid))->find();
		$this->onscroll = true;
		$this->display();
	}
	
	//访问购买记录
	public function getuserbuylist(){
		$type=$_GET['type'];
		$uid=$_GET['uid'];
		$FIdx=$_GET['px'];
		$EIdx=10;//$this->segment(7);
		$isCount=$_GET['ic'];
		$model = new \Think\Model();
		if($type==0){
			//参与购买的商品 全部...
			$shoplist=$model->query("select *,sum(gonumber) as gonumber from `__PREFIX__member_go_record` a left join `__PREFIX__shoplist` b on a.shopid=b.id where a.uid='$uid' GROUP BY shopid ");
	
			$shop['listItems']=$model->query("select *,sum(gonumber) as gonumber from `__PREFIX__member_go_record` a left join `__PREFIX__shoplist` b on a.shopid=b.id where a.uid='$uid' GROUP BY shopid order by a.time desc limit $FIdx,$EIdx " );
		}elseif($type==1){
			//获得奖品
			$shoplist=$model->query("select * from  `__PREFIX__shoplist`  where q_uid='$uid' " );
	
			$shop['listItems']=$model->query("select * from  `__PREFIX__shoplist`  where q_uid='$uid' order by q_end_time desc limit $FIdx,$EIdx" );
		}elseif($type==2){
			//晒单记录
			$shoplist=$model->query("select * from `__PREFIX__shaidan` a left join `__PREFIX__shoplist` b on a.sd_shopid=b.id where a.sd_userid='$uid' " );
	
			$shop['listItems']=$model->query("select * from `__PREFIX__shaidan` a left join `__PREFIX__shoplist` b on a.sd_shopid=b.id where a.sd_userid='$uid' order by a.sd_time desc limit $FIdx,$EIdx" );
	
		}
	
		if(empty($shop['listItems'])){
			$shop['code']=4;
	
		}else{
			foreach($shop['listItems'] as $key=>$val){
				if($val['q_end_time']!=''){
					$shop['listItems'][$key]['codeState']=3;
					$shop['listItems'][$key]['q_user']=get_user_name($val['q_uid']);
					$shop['listItems'][$key]['q_end_time']=microt($val['q_end_time']);
	
				}
				if(isset($val['sd_time'])){
					$shop['listItems'][$key]['sd_time']=date('m月d日 H:i',$val['sd_time']);
				}
			}
			$shop['code']=0;
			$shop['count']=count($shoplist);
		}
		echo json_encode($shop);
	}
	
	public function userrecharge(){
		echo '充值';
	}
	
	//购买记录
	public function userbuylist(){
		$this->isLogin();
		$mysql_model= M('member_go_record');
		$member=$this->userinfo;
		$uid = $member['uid'];
		$this->title="购买记录";
		$totalRows = $mysql_model->where(array('uid' => $uid))->count();
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$pageSize = 20;
		$pageTotal = ceil($totalRows/$pageSize);
		if ($page >= $pageTotal){
			$page = $pageTotal;
		}else {
			if (!$page){
				$page = 1;
			}
		}
		$limit = ($page-1)*$pageSize.','.$pageSize;
		$record = $mysql_model->where(array('uid' => $uid))->limit($limit)->order('id desc')->select();
		$model = M('shoplist');
		foreach ($record as $key => $value) {
			$record[$key]['thumb'] = $model->where(array('id' => $value['shopid']))->getField('thumb');
		}
		
		if (IS_AJAX){
			if (count($record) < $pageSize){
				$count = 0;
			}else{
				$count = 1;
			}
			$this->ajaxReturn(array('status' => 1,'pagetotal' => $pageTotal,'count' => $count,'data' => $record));
		}else{
			$this->record = $record;
		}
		$this->assign('re_active','active');
		$this->display();
	}
	
	public function getgoods(){
		$this->isLogin();
		if (IS_AJAX){
			$orderid = isset($_GET['orderid'])?intval($_GET['orderid']):0;
			if (!$orderid) $this->ajaxReturn(array('status' => 0));
			M('member_go_record')->where(array('id' => $orderid,'uid' => $this->userinfo['uid']))->setField('status','已付款,已发货,已完成');
			$this->ajaxReturn(array('status' => 1));
		}
	}
	
	public function ubuydetail(){
		$this->isLogin();
		$orderid = isset($_GET['gid'])?intval($_GET['gid']):0;
		if (!$orderid){
			$this->error('商品不存在');
		}
		$member=$this->userinfo;
		$this->title="购买详情";
		$record = M('member_go_record')->where(array('id' => $orderid,'uid' => $member['uid']))->find();
		if(!$record){
			$this->error('商品不存在');
		}
		$shopinfo = M('shoplist')->where(array('id' => $record['shopid']))->field(array('thumb','q_end_time'))->find();
		$record['thumb'] = $shopinfo['thumb'];
		$record['q_end_time'] = microt($shopinfo['q_end_time']);
		$this->record = $record;
		$this->display();
	}
	
}