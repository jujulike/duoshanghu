<?php
namespace Go\Controller;

class IndexController extends BaseController{
		
	public function index(){
		$model = M('shoplist');
		$this->okShow = $model->where(array(
			'q_user_code' => array('EXP','is not null'),
			'q_showtime' => 'N',
		))->field($this->field)->order('q_end_time DESC')->limit(4)->select();
		//人气推荐
		$hotRecommend = $model->where(array('renqi' => 1,'pos'=>1,'shenyurenshu' => array('NEQ',0)))->field($this->field)->select();
		$this->hotRecommend = $hotRecommend;
		$this->assign('home_active','active');
		//图片轮播
		$db = M ( 'ads' );
		$date = date( 'Y-m-d', time ());
		$where ['adPositionId'] = '-1';
		$where ['_string'] = " '$date' between  adStartDate  and  adEndDate";
		$adInfo = $db->where ($where )->field ( 'adId,adfile,adurl' )->order ( 'adSort ASC' )->select ();
		$this->assign('adInfo',$adInfo);
		
		$this->display();
	}
	
	private function arrayMore($data,$parentId=0){
		$arr = array();
		foreach ($data as $key => $value){
			if ($value['parentId'] == $parentId){
				$value['child'] = $this->arrayMore($data,$value['catId']);
				$arr[] = $value;
			}
		}
		return $arr;
	}
	
	public function category(){
		$this->title = "商品分类";
		$category = M('goods_cats')->where(array('catFlag' => 1,'isFixed' => 0))->order('catSort asc')->select();
		$category = $this->arrayMore($category);
		$this->assign('category',$category);
		$this->assign('isScroll',true);
		$ads = M('ads')->where(array('adPositionId' => -2))->find();
		$this->assign('ads',$ads);
		$this->display();
	}
	
	public function calresult(){
		$itemid = intval($_GET['gid']);
		$item=M('shoplist')->where(array('id' => $itemid))->find();
		$h=abs(date("H",$item['q_end_time']));
		$i=date("i",$item['q_end_time']);
		$s=date("s",$item['q_end_time']);
		$w=substr($item['q_end_time'],11,3);
		$user_shop_time_add = $h.$i.$s.$w;
		$user_shop_fmod = fmod($user_shop_time_add*100,$item['canyurenshu']);
		
		$time_total = $user_shop_time_add*100;
		$user_shop_time_add = $time_total+$item['opencode'];
		
		if($item['q_content']){
			$item['q_content'] = unserialize($item['q_content']);
			$user_shop_time_add = $item['q_counttime'];
			$user_shop_fmod = fmod($item['q_counttime'],$item['canyurenshu']);
		}
		
		$user_shop_fmod = $item['q_user_code']-10000001;
		$item['picarr'] = unserialize($item['picarr']) ;
		$this->title = '计算结果';
		$this->user_shop_time_add = $user_shop_time_add;
		$this->user_shop_fmod = $user_shop_fmod;
		$item['total_time'] = $time_total;
		$this->item = $item;
		$this->display();
	}
	
	public function item(){
		$goodsId = intval($_GET['gid']);
		if (!$goodsId) $this->error('参数错误');
		$item = M('shoplist')->where(array('id' => $goodsId))->find();
		$this->shopitem = 'itemfun';
		if($item['q_end_time']){
			header("location: ".U('dataserver',array('gid' => $item['id'])));
			exit;
		}
		$model = new \Think\Model();
		$sid=$item['sid'];
		$sid_code = $model->query("select * from `__PREFIX__shoplist` where `sid`='$sid' order by `id` DESC LIMIT 1,1");
		$sid_code = current($sid_code);
		$sid_go_record= $model->query("select * from `__PREFIX__member_go_record` where `shopid`='$sid_code[sid]' and `uid`='$sid_code[q_uid]' order by `id` DESC LIMIT 1");
		
		$title=$item['title'];
		$syrs=$item['zongrenshu']-$item['canyurenshu'];
		$us=$model->query("select * from `__PREFIX__member_go_record` where `shopid`='".$goodsId."' AND `shopqishu`='".$item['qishu']."'ORDER BY id DESC LIMIT 6");
		$itemlist = $model->query("select * from `__PREFIX__shoplist` where `sid`='$item[sid]' and `q_end_time` is not null order by `qishu` DESC");
		
		$arr = explode(',', $item['picarr']);
		if (!empty($arr)){
			$picarr = array();
			foreach ($arr as $k => $v){
				$imgs = explode('@', $v);
				$picarr[] = $imgs[0];
			}
			$item['picarr'] = $picarr;
		}

		//期数显示
		$loopqishu='';
		$loopqishu.='<li class="cur"><a href="javascript:;">'."第".$item['qishu']."期</a><b></b></li>";
		
		if(empty($itemlist)){
			foreach($itemlist as $qitem){
				$loopqishu.='<li><a href="'.U('item',array('gid' => $qitem['id'])).'" class="">第'.$qitem['qishu'].'期</a></li>';
			}}
		
			foreach($itemlist as $qitem){
				if($qitem['id'] == $goodsId){
					$loopqishu.='<li class="cur"><a href="javascript:;">'."第".$itemlist[0]['qishu']."期</a><b></b></li>";
				}else{
					$loopqishu.='<li><a href="'.U('dataserver',array('gid' => $qitem['id'])).'" >第'.$qitem['qishu'].'期</a></li>';
				}
			}
			$gorecode=array();
			if(!empty($itemlist)){
				//查询上期的获奖者信息
				$gorecode=$model->query("select * from `__PREFIX__member_go_record` where `shopid`='".$itemlist[0]['id']."' AND `shopqishu`='".$itemlist[0]['qishu']."' and huode!=0 ORDER BY id DESC LIMIT 1");
			}
			$curtime=time();
			$gorecode = $model->query("select * from `__PREFIX__shoplist` where sid=".$item['sid']." and qishu < ".$item['qishu']." and q_uid is not null and q_showtime='N' order by qishu desc limit 1");
			$gorecode = current($gorecode);
			$gorecode_count=$model->query("select sum(gonumber) as count from `__PREFIX__member_go_record` where `shopid`='".$gorecode['id']."' AND `shopqishu`='".$gorecode['qishu']."' and `uid`='$gorecode[q_uid]'");
			$gorecode_count = current($gorecode_count);
			$gorecode_count=$gorecode_count ? $gorecode_count['count'] : 0;
			$gorecode_time = $model->query("select * from `__PREFIX__member_go_record` where `shopid`='".$gorecode['id']."' AND `shopqishu`='".$gorecode['qishu']."' and `uid`='$gorecode[q_uid]' and `huode` = '$gorecode[q_user_code]' limit 1");
			$this->gorecode_time = current($gorecode_time);
			if ($gorecode){
				$gorecode['q_user'] = unserialize($gorecode['q_user']);
				if (!file_exists($gorecode['q_user']['img'])){
					$gorecode['q_user']['userPhoto'] = 'Upload/member.jpg';
				}
			}
		
		$storeName = M('shops')->where(array('shopId' => $item['shop_id']))->getField('shopName');
		$this->assign('loopqishu', $loopqishu);
		$this->assign('item',$item);
		$this->assign('gorecode',$gorecode);
		$this->title = '商品详情';
		$this->display();
	}
	
	//往期商品查看
	public function dataserver(){
		$this->title="揭晓结果";
		$itemid=intval($_GET['gid']);
		$item = M('shoplist')->where(array('id' => $itemid))->find();
		if(!$item){
			$this->error('商品不存在！');
		}
		if (!$item['q_end_time']) {
			header("location: ".U('item',array('gid' => $item['id'])));
			exit;
		}
		if(empty($item['q_user_code'])){
			$this->success('该商品正在进行中...');
		}
		$itemlist = M('shoplist')->where(array('sid' => $item['sid']))->order('qishu desc')->select();
		//购买中奖码
		$q_user = unserialize($item['q_user']);
		$q_user_code_len = strlen($item['q_user_code']);
		$q_user_code_arr = array();
		for($q_i=0;$q_i < $q_user_code_len;$q_i++){
			$q_user_code_arr[$q_i] = substr($item['q_user_code'],$q_i,1);
		}
		//期数显示
		$loopqishu='';
		if(empty($itemlist[0]['q_end_time'])){
			$loopqishu.='<li><a href="'.U('item',array('gid' => $itemlist[0]['id'])).'">'."第".$itemlist[0]['qishu']."期</a><b></b></li>";
			array_shift($itemlist);
		}
		foreach($itemlist as $qitem){
			if($qitem['id'] == $itemid){
				$loopqishu.='<li class="cur"><a href="javascript:;">'."第".$qitem['qishu']."期</a><b></b></li>";
			}else{
				$loopqishu.='<li><a href="'.U('dataserver',array('gid' => $qitem['id'])).'" >第'.$qitem['qishu'].'期</a></li>';
			}
		}
		//总购买次数
		$user_shop_number = 0;
		//用户购买时间
		$user_shop_time = 0;
		//得到购买码
		$user_shop_codes = '';
		$user_shop_list = M('member_go_record')->where(array('uid' => $item['q_uid'],'shopid' => $itemid,'shopqishu' => $item['qishu']))->select();
		foreach($user_shop_list as $user_shop_n){
			$user_shop_number += $user_shop_n['gonumber'];
			if($user_shop_n['huode']){
				$user_shop_time = $user_shop_n['time'];
				$user_shop_codes = $user_shop_n['goucode'];
			}
		}
		$h=abs(date("H",$item['q_end_time']));
		$i=date("i",$item['q_end_time']);
		$s=date("s",$item['q_end_time']);
		$w=substr($item['q_end_time'],11,3);
		$user_shop_time_add = $h.$i.$s.$w;
		$user_shop_fmod = fmod($user_shop_time_add*100,$item['canyurenshu']);
		if($item['q_content']){
			$item['q_content'] = unserialize($item['q_content']);
		}
		
		$arr = explode(',', $item['picarr']);
		if (!empty($arr)){
			$picarr = array();
			foreach ($arr as $k => $v){
				$imgs = explode('@', $v);
				$picarr[] = $imgs[0];
			}
			$item['picarr'] = $picarr;
		}else{
			$item['picarr'] = $item['thumb'];
		}
		$model = new \Think\Model();
		//记录
		$itemzx=$model->query("select * from `__PREFIX__shoplist` where `sid`='$item[sid]' and `qishu`>'$item[qishu]' and `q_end_time` is null order by `qishu` DESC LIMIT 1");
		$itemzx = current($itemzx);
	    $gorecode = $model->query("select * from `__PREFIX__shoplist` where sid=".$item['sid']." and qishu < ".$item['qishu']." and q_uid is not null and q_showtime='N' order by qishu desc limit 1");
	    $gorecode = current($gorecode);
	    $gorecode_count=$model->query("select sum(gonumber) as count from `__PREFIX__member_go_record` where `shopid`='".$gorecode['id']."' AND `shopqishu`='".$gorecode['qishu']."' and `uid`='$gorecode[q_uid]'");
	    $gorecode_count = current($gorecode_count);
	    $gorecode_count=$gorecode_count ? $gorecode_count['count'] : 0;
	    $gorecode_time = $model->query("select * from `__PREFIX__member_go_record` where `shopid`='".$item['id']."' AND `shopqishu`='".$item['qishu']."' and `uid`='$item[q_uid]' and `huode` = '$item[q_user_code]' limit 1");
	    $gorecode_time = current($gorecode_time);
		if ($gorecode){
				$gorecode['q_user'] = unserialize($gorecode['q_user']);
				if (!file_exists($gorecode['q_user']['img'])){
					$gorecode['q_user']['userPhoto'] = 'Upload/member.jpg';
				}
			}

		$shopitem='dataserverfun';
		$curtime=time();
		//晒单数
		$shopid=$model->query("select * from `__PREFIX__shoplist` where `id`='$itemid'");
		$shopid = current($shopid);
		$shoplist=$model->query("select * from `__PREFIX__shoplist` where `sid`='$shopid[sid]'");
		$shop='';
		foreach($shoplist as $list){
			$shop.=$list['id'].',';
		}
		$id=trim($shop,',');
		if($id){
			$shaidan=$model->query("select * from `__PREFIX__shaidan` where `sd_shopid` IN ($id)");
			$sum=0;
			foreach($shaidan as $sd){
				$shaidan_hueifu=$model->query("select * from `__PREFIX__shaidan_hueifu` where `sdhf_id`='$sd[sd_id]'");
				$sum=$sum+count($shaidan_hueifu);
			}
		}else{
			$shaidan=0;
			$sum=0;
		}
		$itemxq=0;
		if(!empty($itemzx)){
		  $itemxq=1;
		}
		
		$item['q_user'] = unserialize($item['q_user']);
		if (!file_exists($item['q_user']['userPhoto'])){
			$item['uphoto'] = 'Upload/member.jpg';
		}else{
			$item['uphoto'] = $item['q_user']['userPhoto'];
		}
		$member = $this->userinfo;
		$this->item = $item;
		$this->gorecode = $gorecode;
		$this->loopqishu = $loopqishu;
		$this->user_shop_number = $user_shop_number;
		$this->gorecode_count = $gorecode_count;
		$this->user_shop_time = $user_shop_time;
		$this->gorecode_time = $gorecode_time;
		$this->shopitem = $shopitem;
		$this->itemzx = $itemzx;
		$this->display('item');
	}
	
	public function lists(){
		$this->assign('list_active','active');
		$this->assign('title','所有商品');

		$cid = I('cid',0,'intval');
		$cid2 = I('cid2',0,'intval');
		if (!$cid && !$cid2){
			$this->assign('catename','所有商品');
			$this->assign('catid','');
		}else{
			if ($cid && $cid2){
				$getCid = $cid2;
				$this->assign('catid','&cid='.$cid.'&cid2='.$cid2);
			}else{
				$getCid = $cid;
				if (!$getCid){
					$this->error('数据不存在');
				}
				$this->assign('catid','&cid='.$cid);
			}
			$cate = M('goods_cats')->where(array('catId' => $getCid))->find();
			$catename = $cate['catName'];
			$this->assign('catename',$catename);
		}
		if (IS_AJAX){
			$showtype = isset($_GET['type']) ? trim($_GET['type']) : 'default';
			$page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1; 
			$order = "time desc";
			switch ($showtype){
				case 'lastnew':
					$where = '`shenyurenshu`!=0 and `q_end_time` is null';
					$order = "time desc";
					break;
				case 'renqi':
					$where = "`shenyurenshu`!=0 and `pos`=1 and `renqi`=1 and `q_end_time` is null";
					break;
				case 'price':
					$sorts = isset($_GET['sorts'])?$_GET['sorts']:'';
					if (empty($sorts)){
						$sorts = 'asc';
					}
					$where = '`shenyurenshu`!=0 and `q_end_time` is null';
					$order = "money $sorts";
					break;
				case 'cate':
					$where = '`shenyurenshu`!=0 and `q_end_time` is null';
					break;
				default:
					$where = "`shenyurenshu`<= 10 and `q_end_time` is null";
					break;
			}
			
			if ($cid && $cid2){
				$where .= ' and mall_cateid2='.$cid.' and mall_cateid3='.$cid2;
			}else{
				if ($cid && !$cid2){
					$where .= ' and mall_cateid2='.$cid;
				}
			}
			$model = M('shoplist');
			$count = $model->where($where)->count();
			$PageSize = 4;
			$countPage = ceil($count/$PageSize);
			if ($page >= $countPage){
				$page = $countPage;
			}else {
				if ($page <= 1){
					$page = 1;
				}
			}
			$startIndex = ($page-1)*$PageSize;
			$limit = "$startIndex,$PageSize";
			$shoplist = $model->where($where)->field($this->field)->order($order)->limit($limit)->select();
			$start = $page*$PageSize;
			$isNextPage = $model->where($where)->limit("$start,$PageSize")->select();
			$nextPage = count($isNextPage);
			sleep(1);
			$this->ajaxReturn(array('status' => 1,'shoplist' => $shoplist,'isNextPage' => $nextPage));
			exit;
		}
		$this->display();
	}

	public function goodsdesc(){
		$goodsId = intval($_GET['gid']);
		if (!$goodsId) $this->error('参数错误');
		$item = M('shoplist')->where(array('id' => $goodsId))->find();
		$this->assign('title','商品详情');
		$this->assign('item',$item);
		$this->display();
	}
	
	public function buyrecords(){
		$itemid = intval($_GET['gid']);
		if (!$itemid) $this->error('参数错误');
		$cords=M()->query("select * from `__PREFIX__member_go_record` where `shopid`='$itemid'");
		if(!$cords){
			$this->error('暂无购买记录');
		}
		$this->records = $cords;
		$this->item = M('shoplist')->where(array('id' => $itemid))->find();
		$this->title = '购买记录';
		$this->display();
	}
	
	public function lottery(){
		$FIdx=0;
		$EIdx=100;
		$isCount=1;
		$db = new \Think\Model();
		$shopsum=$db->query("select * from `__PREFIX__shoplist` where `q_uid` is not null and `q_showtime` = 'N'");
		//最新揭晓
		$shoplist['listItems']=$db->query("select * from `__PREFIX__shoplist` where `q_uid` is not null and `q_showtime` = 'N' ORDER BY `q_end_time` DESC limit $FIdx,$EIdx");
		
		if(empty($shoplist['listItems'])){
			$shoplist['code']=1;
		}else{
			foreach($shoplist['listItems'] as $key=>$val){
				//查询出购买次数
				$recodeinfo=$db->query("select sum(`gonumber`) as gonumber from `__PREFIX__member_go_record` where `uid` ='$val[q_uid]'  and `shopid`='$val[id]' ");
				$recodeinfo = current($recodeinfo);
				$shoplist['listItems'][$key]['q_user']=get_user_name($val['q_uid']);
				$shoplist['listItems'][$key]['userphoto']=get_user_key($val['q_uid'],'img');
				$shoplist['listItems'][$key]['q_end_time']=microt($val['q_end_time']);
				$shoplist['listItems'][$key]['gonumber']=$recodeinfo['gonumber'];
			}
			$shoplist['code']=0;
			$shoplist['count']=count($shopsum);
		}
		$this->title = '最新揭晓';
		$this->lottery_active = 'active';
		$this->shoplist = $shoplist;
		$this->display();
	}
	
}