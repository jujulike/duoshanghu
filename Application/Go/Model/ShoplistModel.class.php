<?php
namespace Go\Model;
use Think\Model;
class ShoplistModel extends Model {
	
	//添加商品
	public function insert($data){
		if (empty($data)) return false;
		//查询商家状态
		$sql = "select shopStatus from __PREFIX__shops where shopFlag = 1 and shopId=".(int)session('WST_USER.shopId');
		$shopStatus = $this->query($sql);
		if(empty($shopStatus)){
			return false;
		}
		$data['qishu'] = 1;
		$data['q_showtime'] = 'N';
		$zongrenshu = ceil($data['money']/$data['yunjiage']);
		$data['zongrenshu'] = $zongrenshu;
		$data['canyurenshu'] = 0;
		$data['shenyurenshu'] = $zongrenshu;
		if ($zongrenshu == 0){
			return false;
		}
		$data['time'] = NOW_TIME;
		$data['shop_id'] = (int)session('WST_USER.shopId');

		$this->startTrans();
		$query_1 = $this->add($data);
		if (!$query_1) return false;
		$insert_id = $this->getLastInsID();
		$query_table = content_get_codes_table();
		if(!$query_table){
			$this->rollback();
			return false;
		}
		$query_2 = content_get_go_codes($zongrenshu,3000,$insert_id);
		$query_3 = $this->execute("UPDATE `".C('DB_PREFIX')."shoplist` SET `codes_table` = '$query_table',`sid` = '$insert_id',`def_renshu` = '$data[canyurenshu]' where `id` = '$insert_id'");
		if ($insert_id && $query_2 && $query_3){
			$this->commit();
			return true;
			//图片库
			/*
			if (!empty($data['picarr'])){
				$arr = explode(',', $data['picarr']);
				if (!empty($arr)){
					$gallery = array();
					foreach ($arr as $key => $value){
						$imgs = explode('@', $value);
						$gallery[] = array(
							'goodsId' => $insert_id,
							'shopId' => $data['shop_id'],
							'goodsImg' => $imgs[0],
							'goodsThumbs' => $imgs[1]
						);
					}
					M('goods_gallerys')->addAll($gallery);
				}
			}
			*/
			
		}else{
			$this->rollback();
			return false;
		}
	}
	
}