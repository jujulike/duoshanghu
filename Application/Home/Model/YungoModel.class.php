<?php
namespace Home\Model;

class YungoModel extends BaseModel {
	/*************************一元云购首页******************************/
	/**
	 * 获取最新揭晓
	 */
	public function getNewAwary(){
		$FIdx=0;
		$EIdx=100;
		$isCount=1;
		$shopsum=$this->query("select * from `__PREFIX__shoplist` where `q_uid` is not null and `q_showtime` = 'N'");
		//最新揭晓
		$shoplist['listItems']=$this->query("select * from `__PREFIX__shoplist` where `q_uid` is not null and `q_showtime` = 'N' ORDER BY `q_end_time` DESC limit $FIdx,$EIdx");
		if(empty($shoplist['listItems'])){
			$shoplist['code']=1;
		}else{
			foreach($shoplist['listItems'] as $key=>$val){
				//查询出购买次数
				$recodeinfo=$this->query("select sum(`gonumber`) as gonumber from `__PREFIX__member_go_record` where `uid` ='$val[q_uid]'  and `shopid`='$val[id]' ");
				$recodeinfo = current($recodeinfo);
				$shoplist['listItems'][$key]['q_user']=get_user_name($val['q_uid']);
				$shoplist['listItems'][$key]['userphoto']=get_user_key($val['q_uid'],'img');
				$shoplist['listItems'][$key]['q_end_time']=$val['q_end_time'];
				$shoplist['listItems'][$key]['gonumber']=$recodeinfo['gonumber'];
			}
			$shoplist['code']=0;
			$shoplist['count']=count($shopsum);
		}
		return $shoplist;
	}
	//获取人气推荐商品
	public function getHotGoods(){
		$m = M('Shoplist');
		$sql = "select go.id,go.title,go.title2,go.money,go.q_user_code,go.canyurenshu,go.zongrenshu,go.thumb  from __PREFIX__shoplist as go where go.q_end_time is null and go.shenyurenshu > 0  and renqi = 1 and  pos = 1";
		$rs = $m->query($sql);
		//计算参与人数比例
		foreach($rs as $k=>$v){
			$scale = $v['canyurenshu']/$v['zongrenshu']*100;
			$rs[$k]['scale'] = $scale; 
		}
		return $rs;
	}
	//获取所有分类及商品
	public function getAllGoods(){
		$m = M('Shoplist');
		$sql = "select go.id,go.title,go.title2,go.money,go.q_user_code,go.canyurenshu,go.zongrenshu,go.thumb,gc.catName,gc.cateImg,gc.catId from __PREFIX__shoplist as go left join __PREFIX__goods_cats as gc on go.mall_cateid = gc.catId where go.q_end_time is null and go.shenyurenshu >0 and gc.catFlag = 1 order by go.id desc";
		$rs = $m->query($sql);
		//分类
		$cateArray = array();
		foreach($rs as $k=>$v){
			//计算参与人数比例
			$scale = $v['canyurenshu']/$v['zongrenshu']*100;
			$v['scale'] = $scale;
			$cateArray[$v['catName']][] = $v;
		}
		return $cateArray;
	}
	//获取最新上架
	public function getNewGoods(){
		$m = M('Shoplist');
		$sql = "select go.id,go.title,go.title2,go.thumb,go.zongrenshu from __PREFIX__shoplist as go left join __PREFIX__goods_cats as gc on go.mall_cateid = gc.catId where go.q_end_time is null and go.shenyurenshu >0 and gc.catFlag = 1 order by go.id desc";
		$rs = $m->query($sql);
		return $rs;
	}
	//获取揭晓倒计时商品
	public function show_get_awary(){
		$m = M('shoplist');
		$gids = I('gids');
		$str = substr($gids,0,-1);
		$arr = '';
		if(!empty($str)){
			$arr = explode('_',$str);
		}
		$where = array('q_showtime' => 'Y','q_end_time' => array('exp','is not null'));
		if (!empty($arr)){
			$ids = implode(',', $arr);
			$where['id'] = array('NOT IN',$ids);
		}
		//即将揭晓商品
		$data = $m->where($where)->find();
		if (empty($data)){
			$data["status"] = 0;
		}else{
			$data['username'] = get_user_name($data['q_uid']);
			$data["status"] = 1;
			$data["q_end_time"] = intval($data['q_end_time']-time());
		}
		return $data;
	}
	//获取揭晓商品信息
	public function getGoodsInfo(){
		$id = intval(I('gid'));
        if ($id){
            $model = M('shoplist');
            $model->where(array('id' => $id))->setField('q_showtime','N');
            $data = $model->where(array('id' => $id))->find();
            $data['username'] = get_user_name($data['q_uid']);
            $data['status']  =1;
        }else{
        	$data['status'] = 0;
         }
        return $data;
	}
	/*******************************所有商品********************************/
	public function getGoodsList($obj){
		$communityId = I("communityId");
		$c1Id = (int)I("c1Id",0);
		$c2Id = (int)I("c2Id");
		$c3Id = (int)I("c3Id");
		$pcurr = (int)I("pcurr");
		$msort = (int)I("msort",1);//排序标识
		$prices = I("prices");
		if($prices != ""){
			$pricelist = explode("_",$prices);
		}
		$brandId = I("brandId",0);

		$keyWords = urldecode(I("keyWords"));
		$words = array();
		if($keyWords!=""){
			$words = explode(" ",$keyWords);
		}
		$sql = "SELECT  go.id,go.title,go.title2,go.thumb,go.money,go.canyurenshu,go.zongrenshu,go.shenyurenshu,go.yunjiage,go.renqi
				FROM __PREFIX__shoplist go, __PREFIX__shops p ";
		if($brandId>0){
			$sql .=" , __PREFIX__brands bd ";
		}
		$sql .= "where go.shop_id = p.shopId AND go.shenyurenshu > 0 AND go.q_end_time is null";
		if($brandId>0){
			$sql .=" AND bd.brandId=go.brandid AND go.brandid = $brandId ";
		}
		if($c1Id>0){
			$sql .= " AND go.mall_cateid = $c1Id";
		}
		if($c2Id>0){
			$sql .= " AND g.mall_cateid2 = $c2Id";
		}
		if($c3Id>0){
			$sql .= " AND g.mall_cateid3 = $c3Id";
		}
		if(!empty($words)){
			$sarr = array();
			foreach ($words as $key => $word) {
				if($word!=""){
					$sarr[] = "go.title LIKE '%$word%'";
				}
			}
			$sql .= " AND (".implode(" or ", $sarr).")";
		}
	    if($prices != "" && $pricelist[0]>=0 && $pricelist[1]>=0){
			$sql .= " AND (g.shopPrice BETWEEN  ".(int)$pricelist[0]." AND ".(int)$pricelist[1].") ";
		}
		$sql .= " GROUP BY go.id";
		if($msort==1){//最新
			$sql .= " ORDER BY time desc";
		}else if($msort==3){//即将揭晓
			$sql .= " ORDER BY go.shenyurenshu asc ";
		}else if($msort==4){//人气
			$sql .= " ORDER BY go.renqi DESC ";
		}else if($msort==5){//剩余人次
			$sql .= " ORDER BY go.shenyurenshu asc ";
		}else if($msort==6){//价值
			$sql .= " ORDER BY go.money desc";
		}else if($msort==7){//价值
			$sql .= " ORDER BY go.money asc";
		}

		$pages = $this->pageQuery($sql,$pcurr,30);
		foreach($pages['root'] as $k=>$v){
			$scale = $v['canyurenshu']/$v['zongrenshu']*100;
			$rs['root'][$k]['scale'] = $scale; 
		}
		$brands = array();
		$sql = "SELECT b.brandId, b.brandName FROM __PREFIX__brands b, __PREFIX__goods_cat_brands cb WHERE b.brandId = cb.brandId AND b.brandFlag=1 ";
		if($c1Id>0){
			$sql .= " AND cb.catId = $c1Id";
		}
		$sql .= " GROUP BY b.brandId";
		$blist = $this->query($sql);
		for($i=0;$i<count($blist);$i++){
			$brand = $blist[$i];
			$brands[$brand["brandId"]] = array('brandId'=>$brand["brandId"],'brandName'=>$brand["brandName"]);
		}
		$rs["brands"] = $brands;
		$rs["pages"] = $pages;
		$gcats["mall_cateid"] = $c1Id;
		$gcats["mall_cateid2"] = $c2Id;
		$gcats["mall_cateid3"] = $c3Id;
		$rs["goodsNav"] = self::getgoodsNav();
		return $rs;
	}
	//获取所有商品分类
	public function getgoodsNav(){
		$m = M('Shoplist');
		$sql = "select gc.catName,gc.cateImg,gc.catId from __PREFIX__shoplist as go left join __PREFIX__goods_cats as gc on go.mall_cateid = gc.catId where go.shenyurenshu > 0 and gc.catFlag = 1 group by gc.catId";
		$rs = $m->query($sql);
		return $rs;
	}

	/********************************获取商品详情******************************/
	//获取商品详情
	public function getGoodsDetails(){
		$id = intval(I('gid'));
		$goods = array();
		//获取商品信息
		$sql = "select go.* from __PREFIX__shoplist as go where go.q_end_time is null and go.shenyurenshu > 0 and go.id = ".$id;
		$goods['details'] = $this->queryRow($sql);
		$goods['details']['content'] = htmlspecialchars_decode($goods['details']['content']);
		//计算参与人数比例
		$goods['details']['scale'] = $goods['details']['canyurenshu']/$goods['details']['zongrenshu']*100;
		//获取商品图册
		if (!empty($goods['details']['picarr'])){
			$arr = explode(',', $goods['details']['picarr']);
			if (!empty($arr)){
				$gallery = array();
				foreach ($arr as $key => $value){
					$imgs = explode('@', $value);
					@$gallery[] = array('goodsThumbs' => $imgs[1],'goodsImg' => $imgs[0]);
				}
			}
		}
		$goods['details']['gallery'] = $gallery;
		//获取分类信息
		$data = array();
		foreach($goods['details'] as $k=>$v){
			if($k == 'mall_cateid' || $k == 'mall_cateid2' || $k == 'mall_cateid3'){
				$sql = "select catId,catName from __PREFIX__goods_cats where catId = ".$v;
				$data[$k] = $this->queryRow($sql);
			}
		}
		$goods['cat'] = $data;
		//获取其他期数
		if($goods['details']['qishu'] > 1){
			$sql = "select id,qishu from __PREFIX__shoplist where sid = ".$goods['details']['sid']." and q_end_time is not null order by qishu desc";
			$goods['qishu'] = $this->query($sql);
		}
		//查询上期的获奖者信息
		$goods['record']=$this->queryRow("select * from `__PREFIX__member_go_record` where `shopid`='".$goods['qishu'][0]['id']."' AND `shopqishu`='".$goods['qishu'][0]['qishu']."' and huode!=0");
		$m = M('Shoplist');
		$goods['record']['q_end_time'] = $m->where('id = '.$goods['qishu'][0]['id'])->getField('q_end_time');
		return $goods;
	}
	/**************************一元云购购物清单*****************************/
	//获取清单商品列表
	public function getCartList(){
		$cartList = json_decode(stripslashes(cookie('Cartlist')),true);
		$cartGoodsList = array();
		foreach($cartList as $k=>$v){
			$sql = "select go.id,go.title,go.title2,go.money,go.yunjiage,go.canyurenshu,go.zongrenshu,go.thumb,go.qishu,go.shenyurenshu  from __PREFIX__shoplist as go left join __PREFIX__goods_cats as gc on go.mall_cateid = gc.catId where go.id = ".$k." and gc.catFlag = 1";
			$rs = $this->queryRow($sql);
			if($v['num'] >= $rs['shenyurenshu']){
				$rs['num'] = $rs['shenyurenshu'];
			}else{
				$rs['num'] = $v['num'];
			}
			$cartGoodsList[] = $rs;
		}
		//计算剩余人次及商品总金额
		$moneyCount = 0;
		foreach($cartGoodsList as $k=>$v){
			$shenyu = $v['zongrenshu']-$v['canyurenshu'];
			$cartGoodsList[$k]['shenyu'] = $shenyu;
			$moneyCount += $v['yunjiage']*$v['num'];
		}
		$cartGoodsList['MoneyCount'] = sprintf('%.2f',$moneyCount);
		return $cartGoodsList;
	}
	//获取推荐商品
	public function getPosGoods(){
		$m = M('Shoplist');
		$sql = "select go.id,go.title,go.title2,go.money,go.q_user_code,go.canyurenshu,go.zongrenshu,go.thumb  from __PREFIX__shoplist as go left join __PREFIX__goods_cats as gc on go.mall_cateid = gc.catId where go.q_end_time is null and go.shenyurenshu >0 and gc.catFlag = 1 order by go.id desc";
		$rs = $m->query($sql);
		return $rs;
	}
	//增加或减少商品数量
	public function changeNum(){
		$rd = array('status'=>-1);
		$id = intval(I('gid',0));
		$num = intval(I('num',0));
		$cartList = json_decode(stripslashes(cookie('Cartlist')),true);
		if($id > 0){
			if(is_array($cartList)){
				foreach($cartList as $k=>$v){
					if($k == $id){
						//增加
						if($num == 1){
							$cartList[$k]['num']++;
						//减少
						}else if($num == -1){
							$cartList[$k]['num']--;
						}
						$rd['status'] = 1;
						cookie('Cartlist',json_encode($cartList));
					}
				}
			}
		}
		return $rd;
	}
	//修改商品数量
	public function changeNums(){
		$rd = array('status'=>-1);
		$id = intval(I('gid',0));
		$num = intval(I('num',0));
		$cartList = json_decode(stripslashes(cookie('Cartlist')),true);
		if($id > 0){
			if(is_array($cartList)){
				foreach($cartList as $k=>$v){
					if($k == $id){
						$cartList[$k]['num'] = $num;
						$rd['status'] = 1;
						cookie('Cartlist',json_encode($cartList));
					}
				}
			}
		}
		return $rd;
	}
	//删除选中清单商品
	public function delSelete(){
		$rd = array('status'=>-1);
		$ids = I('ids');
		$cartList = json_decode(stripslashes(cookie('Cartlist')),true);
		if(!empty($ids)){
			if(is_array($cartList)){
				foreach($cartList as $k=>$v){
					if(in_array($k,$ids)){
						$rd['status'] = 1;		
					}else{
						$Cartlist[$k]['num'] = $v['num'];
					}
				}
				cookie('Cartlist',json_encode($CartList));
			}
		}
		return $rd;
	}

	/***************************************一元云购在线支付*****************************/
	//获取即将揭晓商品
	public function getRightNow(){
		$m = M('Shoplist');
		$sql = 'select id,title,title2,thumb,money,zongrenshu,canyurenshu,qishu,yunjiage from __PREFIX__shoplist where shenyurenshu <> 0 order by canyurenshu desc limit 4';
		$rightNowGoods = $m->where(array(
			'q_user_code' => array('EXP','is not null'),
			'q_showtime' => 'N',
		))->order('q_end_time DESC')->limit(4)->select();
		return $rightNowGoods;
	}
	/*********************************买家中心*******************************/
	//获取购买记录
	public function getUserBuyList($uid){
		$code = I('code');
		$shopname = I('shopname');
		$status = intval(I('status',-999));
		$m= M('member_go_record');
		$sql = "select * from __PREFIX__member_go_record where uid = ".$uid;
		if($code != ''){$sql .= " and code like '%$code%'";}
		if($shopname != ''){$sql .= " and shopname like '%$shopname%'";}
		$sql .= " order by id desc";
		$rs = $m->pageQuery($sql);
		$m = M('shoplist');
		$data = array();
		foreach ($rs['root'] as $key => $value) {
			$where = "id = ".$value['shopid'];
			if($status == 1){
				$where .= ' and q_end_time is null';
			}else if($status == 2){
				$where .= ' and q_end_time is not null';
			}
			$shoplistId = $m->where($where)->getField('id');
			if($shoplistId != $value['shopid']){
				continue;
			}
            $rs['root'][$key]['thumb'] = $m->where($where)->getField('thumb');
            $rs['root'][$key]['title2'] = $m->where($where)->getField('title2');
            $rs['root'][$key]['q_showtime'] = $m->where($where)->getField('q_showtime');
            $q_end_time = $m->where($where)->getField('q_end_time');
            $rs['root'][$key]['q_end_time'] = $q_end_time;
            if($q_end_time != null){
            	$rs['root'][$key]['to'] = 5;
            }else{
            	$rs['root'][$key]['to'] = 4;
            } 
            $data[] = $rs['root'][$key]; 
        }
        $rs['root'] = $data;
        $rs['total'] = count($data);
        $rs['totalPage'] = (int)ceil($rs['total']/$rs['pageSize']);
		return $rs;
	}
	//获取购买详情
	public function getYungoDetails($uid,$orderid){
		$m = M('member_go_record');
		//获取购买信息
		$rs = $m->where(array('id' => $orderid,'uid' => $uid))->find();
		if(!$rs){
			return false;
		}else{
			//获取商品信息
			$m = M('shoplist');
			$shopInfo = $m->where(array('id' => $rs['shopid']))->find();
			$rs['thumb'] = $shopInfo['thumb'];
			$rs['title2'] = $shopInfo['title2'];
			$rs['q_end_time'] = $shopInfo['q_end_time'];
			if($rs['q_end_time'] != null){
				$rs['to'] = 5;
			}else{
				$rs['to'] = 4;
			}
			if(!$shopInfo['q_uid']){
				$rs['ren']='还未开奖';
				$rs['ma']='还未开奖';
				$rs['time'] = '1';
			}else{
				$rs['ren']=get_user_name($shopInfo['q_uid']);
				$rs['ma']=$shopInfo['q_user_code'];
			}
			if($shopInfo['q_uid'] == $uid){
				$rs['isWinning'] = 1;
			}
			//获取用户默认收货地址
			$m = M('UserAddress');
			$info = $m->where(array('userId' => $uid,'isDefault' => 1))->find();
			if(!empty($info)){
				$sheng = M('areas')->where(array('areaId' => $info['areaId1']))->getField('areaName');
				$shi = M('areas')->where(array('areaId' => $info['areaId2']))->getField('areaName');
				$xian = M('areas')->where(array('areaId' => $info['areaId3']))->getField('areaName');
				$shequ = M('communitys')->where(array('communityId' =>$info['communityId']))->getField('communityName');
				$rs['userName'] = $info['userName'];
				$rs['userPhone'] = $info['userPhone'];
				$rs['address'] = $sheng.'-'.$shi.'-'.$xian.'-'.$shequ.'-'.$info['address'];
			}
			
		}
		return $rs;
	}

	//获取中奖记录
	public function getWinningRecord($uid){
		$code = I('code');
		$shopname = I('shopname');
		$status = intval(I('status',-999));
		$sql = "select * from `__PREFIX__member_go_record` a left join `__PREFIX__shoplist` b on a.shopid=b.id where b.q_uid=$uid and b.q_showtime='N' ";
		if($code != ''){$sql .= " and a.code like '%$code%'";}
		if($shopname != ''){$sql .= " and a.shopname like '%$shopname%'";}
		$rs = $this->query($sql);
		$sql = "select a.*,b.title,b.title2,b.q_end_time as end_time,b.thumb from `__PREFIX__member_go_record` a left join `__PREFIX__shoplist` b on a.shopid=b.id where b.q_uid=$uid and a.huode!=0  and b.q_showtime='N' ";
		if($code != ''){$sql .= " and code like '%$code%'";}
		if($shopname != ''){$sql .= " and shopname like '%$shopname%'";}
		$sql .= " order by a.time desc";
		$orderlist['listItems'] = $this->pageQuery($sql);
		if(empty($orderlist['listItems'])){
			$orderlist['code'] = 1;
		}else{
			foreach($orderlist['listItems']['root'] as $k=>$v){
				$orderlist['listItems']['root'][$k]['q_end_time'] = microt($v['end_time']);
			}
			$orderlist['code'] = 0;
		}
		$orderlist['count'] = count($rs);
		return $orderlist;
	}
	//确认收货
	public function yungoConfirm($uid){
		$rd = array('status'=>-1);
		$id = intval(I('id',0));
		if(!$id){
			return $rd;
		}else{
			$m = M('member_go_record');
			$rs = $m->where('id = '.$id.' and 	uid = '.$uid)->setField('status','已付款,已发货,已完成');
			$rd['status'] = 1;
		}
		return $rd;
	}
}