<?php
namespace Home\Controller;
/**
 * 提现
 */
class ApplyController extends BaseController {
    public function __construct(){

        parent::__construct();



    }

    

    public function payview(){

        $m = D('Home/Apply');
        $info = $m->userInfo();
        $this->assign('user',$info);
        $this->display("Apply/payview");

    }


    public function shopPayView(){

        $m = D('Home/Apply');
        $info = $m->shopsInfo();
        $this->assign('user',$info);
        $this->display("Apply/shopPayView");

    }


    public function apply(){

        $m = D('Home/Apply');
        $data = $m->checkApply();



        $this->ajaxReturn($data);
    }

    public function shopsIndex(){




        $applyLogdata = D('Home/Apply')->ApplyShopData();
        $income = D('Home/Apply')->incomeShopData();
        $incomeCount = D('Home/Apply')->incomeCount();

        $this->assign('income',json_encode($income));
        $this->assign('incomeCount',$incomeCount);
        $this->assign('applyLog',json_encode($applyLogdata));
        $this->assign("umark","shopsIndex");
        $this->display("Apply/shopsIndex");

    }

    public function usersIndex(){


        $applyLogdata = D('Home/Apply')->ApplyUserData();

        $this->assign('applyLog',json_encode($applyLogdata));
        $this->assign("umark","usersIndex");
        $this->display("Apply/usersIndex");

    }



    public function applyShop(){

        $m = D('Home/Apply');
        $data = $m->checkShopApply();



        $this->ajaxReturn($data);
    }
    
    public  function Balance(){

        $m = D('Home/Apply');
        $data = $m->checkBalance();




        echo (json_encode($data));
    }
    
    public  function checkShopBalance(){

        $m = D('Home/Apply');
        $data = $m->checkShopBalance();




        echo (json_encode($data));
    }

    public function applyPs(){


        $m = D('Home/Apply');
        $data = $m->CheckApplyPassword();



        echo (json_encode($data));
    }

    

    

	
}