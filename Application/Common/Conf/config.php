<?php
define('SHOP_DIY',false);
 return array(
	    'VAR_PAGE'=>'p',
	    'PAGE_SIZE'=>15,
		'DB_TYPE'=>'mysql',
	    'DB_HOST'=>'localhost',
	     'DB_NAME'=>'duoshanghu',
	    'DB_USER'=>'root',
	    'DB_PWD'=>'111111',
	    'DB_PREFIX'=>'pub_',
	    'DEFAULT_CITY' => '440100',
 		'URL_MODEL' => 2,
	    'DATA_CACHE_SUBDIR'=>true,
        'DATA_PATH_LEVEL'=>2, 
	    'SESSION_PREFIX' => 'e_mall',
        'COOKIE_PREFIX'  => 'e_mall',
		'LOAD_EXT_CONFIG' => 'ext_config',
 		'MODULE_ALLOW_LIST' => array('Wx','Admin','Home','Go','Api','Store'),    // 允许访问的模块列表
 		'DEFAULT_MODULE' => 'Wx',
 		//图片上传相关信息
		'VIEW_ROOT_PATH'        =>   '/Upload/',
		/* 自动运行配置 */
	    'CRON_CONFIG_ON' => false, // 是否开启自动运行
	    'CRON_CONFIG' => array(
	        '团购活动倒计时' => array('Home/Base/changeGroupStatus', '1', ''), //路径(格式、间隔秒（0为一直运行）、开始时间
	    ),
	);
?>