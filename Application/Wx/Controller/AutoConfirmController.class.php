<?php
namespace Wx\Controller;
use Think\Controller;
use Think\Model;

class AutoConfirmController extends BaseController
{
	
	public function index(){
		echo 'testing';
	}

    //供首页调用的自动确认收货
    public  function indexAutoConfirm($orderInfo){

        //更新商家金额
        M()->startTrans();

        //商家收入记录

        $A =  $this->proportion($orderInfo['shopId'],$orderInfo['orderId']);

        $C=$this->scoreRecord(1,$orderInfo['needPay'],$orderInfo['orderNo'],1,$orderInfo['userId']);
        $D=M('orders')->where(array('orderId'=>$orderInfo['orderId'],'userId'=>$orderInfo['userId']))->setField(array('orderStatus'=>4,'signTime'=>date("Y-m-d H:i:s"),'isConfirm'=>1));
        $E=$this->confirmOrder_log($orderInfo['orderId']);

        if($A&&$C&&$D&&$E){
            M()->commit();
            //写入自动收货日志
            $ym=date('Y-m');
            $logfile='./data/auto_confirm_log_'.$ym.'.log';
            if(file_exists($logfile)){
                @file_put_contents($logfile,$orderInfo['orderId'].'|'.date('Y-m-d H:i:s')."\n",FILE_APPEND);
            }else{
                $myfile = fopen($logfile, "a+");;
                $txt = $orderInfo['orderId'].'|'.date('Y-m-d H:i:s')."\n";
                fwrite($myfile, $txt);
                fclose($myfile);
            }
            return true;
        }else{
            M()->rollback();
            return false;
        }

    }

    // 积分操作记录
    /**
     * 构造函数
     * @param $type 1购物，2取消订单，3充值，4订单无效，5活动,6评价订单
     * @param $score 积分
     * @param $shopid 店铺ID
     * @param $orderid 订单ID或者充值ID
     * @param $IncDec 积分变动0为减，1加
     * @param $userid 用户ID
     * @param $totalscore 用户剩余总积分
     */
    public function scoreRecord($type = '', $payMoney=0, $orderNo = '', $IncDec = '', $userid = 0) {
        $score=floor($payMoney);
        if($score<=0){
            return;
        }
        $totalscore=M('users')->where(array('userId'=>$userid))->getField('userScore');
        $db = M ( 'score_record' );
        $data ['score'] = $score;
        $data ['type'] = $type;
        $data ['time'] = time ();
        $data ['ip'] = get_client_ip ();
        $data ['orderNo'] = $orderNo;
        $data ['IncDec'] = $IncDec;
        $data ['userid'] = $userid;
        $data ['totalscore'] = $totalscore;
        $res = $db->add ( $data );
        return $res;
    }
    public function confirmOrder_log($id){
        $userId=M('orders')->where(array('orderId'=>$id))->getField('userId');
        $data = array();
        $data["orderId"] = $id;
        $data["logContent"] = "自动确认收货完成";
        $data["logUserId"] = $userId;
        $data["logType"] = 0;
        $data["logTime"] = date('Y-m-d H:i:s');
        $mlogo = M('log_orders');
        $re = $mlogo->add($data);
        return $re;
    }

    public function proportion($shopId,$ordersId){
        $res = false;
        $proportion = M('shops')->where(array('shopId'=>$shopId))->getField('proportion');

        $orderInfo = M('orders')->field('userId,needPay')->where(array('orderId'=>$ordersId,'isPay'=>1,'payType'=>array('neq','0')))->find();
        $pushAdmin = $proportion*$orderInfo['needPay'];
        $pushShop =  $orderInfo['needPay']-$pushAdmin;

        $data['orderId'] = $ordersId;
        $data['proportion'] = $proportion;
        $data['userId'] = $orderInfo['userId'];
        $data['money']   = $orderInfo['needPay'];
        $data['time']=date('Y-m-d H:i:s',time());
        $data['shopId']=$shopId;
        $data['pushShop']=$pushShop;
        $data['pushAdmin']=$pushAdmin;

        $log = M('biz_income')->add($data);

        $s=  M('shops')->where(array('shopId'=>$shopId))->setInc('bizMoney',$pushShop);
        if($log&&$s){
            $res = true;
        }

        return $res;
    }
}
