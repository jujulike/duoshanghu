<?php
namespace Wx\Controller;

use Think\Model;
use Wx\Controller\BaseController;

class UcenterController extends BaseController
{
    
    public function _initialize()
    {
        parent::isLogin();
        @header('Content-type: text/html;charset=UTF-8');
    }
    // 积分
    public function score()
    {
        parent::isLogin();
        $map['userId'] = session('oto_userId');
        $this->score = M('users')->where($map)->getField('userScore');
        $month['userid'] = session('oto_userId');
        $month['_string'] = "date_sub(curdate(), INTERVAL 30 DAY) <= FROM_UNIXTIME(time)";
        $this->scoreList = M('score_record')->field('score,time,IncDec,type')
            ->where($month)->order('id desc')
            ->select();
        $this->display();
    }
    // 钱包
    public function wallet()
    {
        parent::isLogin();
        $map['userId'] = session('oto_userId');
        $this->balance = M('users')->where($map)->getField('userMoney');
        /*
        $field = "m.type as actionType,m.money,m.time,m.orderNo,m.balance,m.payWay,m.IncDec,s.shopImg";
        $bmap['m.userid'] = session('oto_userId');
        $balanceList = M('money_record')->where($bmap)
        ->join('as m left join oto_orders as o on o.orderNo=m.orderNo join oto_shops as s on s.shopId=o.shopId')
        ->field($field)->select();
        */
        
        $user_id=session('oto_userId');
        $sql="select *,`type` as actionType from `oto_money_record` where `userid`='".$user_id."' and type!='0' order by `time` DESC";
        $user_info=M()->query($sql);
        for($i=0;$i<count($user_info);$i++)
        {
            if($user_info[$i]['type']==1 || $user_info[$i]['type']==2)
            {
                $order_id=$user_info[$i]['orderNo'];
                $sql="select * from `oto_orders`,`oto_shops` where oto_orders.shopId=oto_shops.shopId and `orderNo`='".$order_id."'";
                $order_shop_info[$i]=M()->query($sql);
                if(!isset($order_shop_info[$i]))
                {
                    $user_info[$i]['logo']='Upload/shanghu_logo.png';
                    $user_info[$i]['pan']=1;
                }
                else
                {
                    $user_info[$i]['logo']=$order_shop_info[$i][0]['shopImg'];
                    $user_info[$i]['pan']=0;
                }
            }
            else if($user_info[$i]['type']==3)
            {
                if($user_info[$i]['payWay']==0) $user_info[$i]['logo']='Upload/shanghu_logo.png';
                if($user_info[$i]['payWay']==1) $user_info[$i]['logo']='Upload/zhifubao.png';
                if($user_info[$i]['payWay']==2) $user_info[$i]['logo']='Upload/weixin.png';
                $user_info[$i]['pan']=1;
            }
            else
            {
                $user_info[$i]['logo']='Upload/shanghu_logo.png';
            }
            if($user_info[$i]['remark']=="")
            {
                if($user_info[$i]['type']==1) $user_info[$i]['remark']='购买商品';
                else if($user_info[$i]['type']==2) $user_info[$i]['remark']='订单退款';
                else if($user_info[$i]['type']==3)
                {
                    if($user_info[$i]['payWay']==1) $user_info[$i]['remark']='支付宝充值';
                    else if($user_info[$i]['payWay']==2) $user_info[$i]['remark']='微信充值';
                    else $user_info[$i]['remark']='其他充值方式';
                } 
                else if($user_info[$i]['type']==4) $user_info[$i]['remark']='佣金提现';
                else if($user_info[$i]['type']==5) $user_info[$i]['remark']='订单无效';
                else $user_info[$i]['remark']='其他';
            }
        }
        $balanceList=$user_info;
        foreach ($balanceList as $k=>$v){
            if($v['actionType']==0){
                $balanceList[$k]['yongtu']='其它';
            }else if($v['actionType']==1){
                $balanceList[$k]['yongtu']='购物消费';
            }else if($v['actionType']==2){
                $balanceLis[$k]['yongtu']='取消订单';
            }else if($v['actionType']==3){
                $balanceList[$k]['yongtu']='充值';
            }else if($v['actionType']==4){
                $balanceList[$k]['yongtu']='提现';
            }else if($v['actionType']==5){
                $balanceList[$k]['yongtu']='无效订单';
            }else{
                $balanceList[$k]['yongtu']='其它';
            }
        }
        $this->balanceList=$balanceList;
        $this->display();
    }
    //充值
    public function topUp(){
        $this->display();
    }
}