<?php
 namespace Admin\Model;
/**
 * 论坛帖子模型
 */
class PostsModel extends BaseModel {
	/****************帖子分类*****************/
    //新增分类 
    public function addCate(){
        $name=I('catName');
        $db=M('posts_cate');
        if(!$name){
            return array('status'=>-1);
        }
        $data['name']=$name;
        $data['sort']=I('catSort',0);
        $data['isShow']=I('isShow',1);
        $res=$db->add($data);
        if($res!=false){
            return array('status'=>1);
        }else{
            return array('status'=>-1);
        }
    }
    //查看分类
    public function getCate(){
        $info=M('posts_cate')->order('sort ASC')->select();
        $newArr=$this->cateHandle($info);
        return $newArr;
    }
    
    public function cateHandle($arr,$pid=0){
        $newArr=array();
        foreach ($arr as $k=>$v){
            if($v['pid']==$pid){
                $v['child']=self::cateHandle($arr,$v['cid']);
                $newArr[]=$v;
            }
        }
        return $newArr;
    }
    
    //排序处理
    public  function sortHandle(){
        $sort=I('val');
        $id=I('id');
        $db=M('posts_cate');
        if(!$id){
            return array('status'=>-1);
        }
        if(!is_numeric($sort)){
            return array('status'=>-1);
        }
        $data['cid']=$id;
        $data['sort']=$sort;
        $res=$db->save($data);
        if($res!=false){
            return array('status'=>$sort);
        }else{
            return array('status'=>-1);
        }
    }
    
    //删除分类 
    public function delCate(){
        $pid=I('pid');
        $id=I('id');
        if(!$id){
            return array('status'=>-1);
        }
        $db=M('posts_cate');
        if($pid==0){
            $A=$db->where(array('cid'=>$id))->delete();
            $B=$db->where(array('pid'=>$id))->delete();
            if($A!==false&&$B!==false){
                    return array('status'=>0);
            }else{
                    return array('status'=>-1);
             }
        }else{
            $res=$db->where(array('cid'=>$id))->delete();
            if($res!=false){
                return array('status'=>0);
            }else{
                return array('status'=>-1);
            }
        }
      
    }
    //修改帖子分类显示情况
    public function showHandle(){
        $pid=I('pid');
        $id=I('id');
        $show=I('show')==1?I('show'):0;
        if(!$id){
            return array('status'=>-1);
        }
        $db=M('posts_cate');
        if($pid==0){
            $A=$db->where(array('cid'=>$id))->setField('isShow',$show);
            $B=$db->where(array('pid'=>$id))->setField('isShow',$show);
            if($A!==false&&$B!==false){
                return array('status'=>0);
            }else{
                return array('status'=>-1);
            }
        }else{
            $res=$db->where(array('cid'=>$id))->setField('isShow',$show);
            if($res!=false){
                return array('status'=>0);
            }else{
                return array('status'=>-1);
            }
        }
        
    }

	/****************帖子列表*****************/
    /**
	 * 分页查看帖子列表
	 */
	 public function queryByPage(){
	 	$title = I('title');
	 	$content = I('content');
	 	$userName = I('userName');
	 	$status = I('status',-1);
        $cid = I('cid',-1);
	 	$sql = "select p.*,u.userName,t.name as cateName from __PREFIX__posts as p left join __PREFIX__users as u on p.userId = u.userId left join oto_posts_cate as t on t.cid=p.cid where p.isDel = 0 and t.isShow = 1";
	 	if($title!=''){$sql .= " and p.title like '%$title%'";}
	 	if($content!=''){$sql .= " and p.content like '%$content%'";}
	 	if($userName!=''){$sql .= " and (u.userName like '%$userName%' or u.userName like '%$userName%')";}
	 	if($status>=0){$sql .= " and p.status = $status";}
        if($cid>=0){$sql .= " and t.cid = $cid";}
	 	$sql .=" order by id desc";
	 	$page = $this->pageQuery($sql);
        //获取所有分类
        $postsCModel = M('PostsCate');
        $page['postCate'] = $postsCModel->where('isShow = 1')->order('sort asc')->select();
	 	return $page;
	 }
	/**
	* 修改帖子状态
	*/
	public function editStatus(){
		$rd = array('status'=>-1);
		if(I('id',0)==0)return $rd;
		$status = (int)I('status');
		$id = (int)I('id');
		$this->status = ($status==1)?1:0;
		$rs = $this->where("id = {$id}")->save();
		if(false !== $rs){
		    $rd['status']= 0;
		}
		return $rd;
	} 
	//删除单个帖子
    public  function delPosts(){
        $id=I('id');
        if(!$id){
            return  array('status'=>-1);
        }
        $res=M('posts')->where(array('id'=>$id))->setField(array('isDel'=>1));
        if($res!=false){
            //删除评论
            M('posts_comment')->where(array('psId'=>$id))->setField(array('isDel'=>1));
            //删除举报
            M('posts_inform')->where(array('psId'=>$id))->setField(array('isDel'=>1));
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
        
    }
    //批量删除帖子
    public  function delSelectPosts(){
        $id = self::formatIn(",", I('id',0));
        if(!$id){
            return  array('status'=>-1);
        }
        $res=M('posts')->where("id in({$id})")->setField(array('isDel'=>1));
        if($res!=false){
            //删除评论
            M('posts_comment')->where(array('psId'=>array('in',$id)))->setField(array('isDel'=>1));
            //删除举报
            M('posts_inform')->where(array('psId'=>array('in',$id)))->setField(array('isDel'=>1));
            
            return  array('status'=>0);
            
        }else{
            return  array('status'=>-1);
        }
        
    }
    //批量隐藏帖子
    public  function hideSelectPosts(){
        $id = self::formatIn(",", I('id',0));
        if(!$id){
            return  array('status'=>-1);
        }
        $res=M('posts')->where("id in({$id})")->setField(array('status'=>0));
        if($res!=false){
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
        
    }
    //批量显示帖子
    public  function showSelectPosts(){
        $id = self::formatIn(",", I('id',0));
        if(!$id){
            return  array('status'=>-1);
        }
        $res=M('posts')->where("id in({$id})")->setField(array('status'=>1));
        if($res!=false){
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
        
    }

	/******************帖子详情****************/
	//查看帖子详情
    public function toDetail(){
        $id=I('id');
        $sql="select p.*,u.userName,u.userName,t.name as cateName from __PREFIX__posts as p left join oto_users as u on u.userId=p.userId  left join oto_posts_cate as t on t.cid=p.cid  where p.id=$id and isDel=0 ";
        $posts=$this->query($sql);
        $sql = "select c.*,u.userName from __PREFIX__posts_comment as c left join oto_users as u on u.userId=c.userId where c.psId = $id and c.isDel = 0";
        $comment = $this->pageQuery($sql);
        return array('posts'=>$posts[0],'comm'=>$comment);
    }
    //修改评论状态
    public  function showHideComment(){
        $id=I('id');
        if(!$id){
            return  array('status'=>-1);
        }
        $status=I('status')?I('status'):0;
        $res=M('posts_comment')->where(array('id'=>$id))->setField(array('status'=>$status));
        if($res!=false){
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
    }
    //批量隐藏帖子评论
    public  function hideSelectComment(){
        $id = self::formatIn(",", I('id',0));
        if(!$id){
            return  array('status'=>-1);
        }
        $res=M('posts_comment')->where("id in({$id})")->setField(array('status'=>0));
        if($res!=false){
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
        
    }
    //批量显示帖子评论
    public  function showSelectComment(){
        $id = self::formatIn(",", I('id',0));
        if(!$id){
            return  array('status'=>-1);
        }
        $res=M('posts_comment')->where("id in({$id})")->setField(array('status'=>1));
        if($res!=false){
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
    }

    /******************评论管理****************/
        public function Comment(){
        $username=I('userName');
        $content=I('content');
        $title=I('title');
        $status=I('status',-1);
        $where=" where pc.isDel=0";
        if($username!=''){$where.=" and u.userName like '%$username%' ";}
        if($title!=''){$where.=" and p.title like '%$title%'";}
        if($content!=''){$where.=" and pc.content  like '%$content%'" ;}
        if($status>=0){$where .= " and pc.status = $status";}
        $sql="select pc.*,u.userName,p.title from (__PREFIX__posts_comment as pc left join __PREFIX__users as u on u.userId=pc.userId left join __PREFIX__posts as p on p.id = pc.psId),__PREFIX__posts_cate as pcs $where and pcs.cid = p.cid and pcs.isShow = 1 order by pc.id desc";
        $info=$this->pageQuery($sql);
        //获取回复对象用户名
        foreach($info['root'] as $k=>$v){
            $postsCModel = M('PostsComment');
            $objectUserId= $postsCModel->where('id = '.$v['pid'])->getField('userId');
            $usersModel = M('Users');
            $objectUserName = $usersModel->where('userId = '.$objectUserId)->getField('userName');
            $info['root'][$k]['objectUserName'] = $objectUserName;
        }
        return $info;
    }
    //修改评论显示状态
    public function editCommentStatus(){
        $postsCModel = M('posts_comment');
        $rd = array('status'=>-1);
        if(I('id',0)==0)return $rd;
        $status = (int)I('status');
        $id = (int)I('id');
        $postsCModel->status = ($status==1)?1:0;
        $rs = $postsCModel->where("id = {$id}")->save();
        if(false !== $rs){
            $rd['status']= 0;
        }
        return $rd;
    }
    //删除单个评论
    public  function delComment(){
        $id=I('id');
        //评论数
        $num=M('posts_comment')->where(array('pid'=>$id))->count();
        $num=$num+1;
        $psid=M('posts_comment')->where(array('id'=>$id))->getField('psId');
        $res=M('posts_comment')->where(array('id'=>$id))->setField(array('isDel'=>1));
        if($res!=false){
            //删除评论下面的回复
            M('posts_comment')->where(array('pid'=>$id))->setField(array('isDel'=>1));
            //更新评论数
            M('posts')->where(array('id'=>$psid))->setDec('comNum',$num);
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
    }
    //批量删除评论
    public  function delSelectComment(){
        $id = self::formatIn(",", I('id',0));
        if(!$id){
            return  array('status'=>-1);
        }
        
        $res=M('posts_comment')->where("id in({$id})")->setField(array('isDel'=>1));
        if($res!=false){
            $ids=explode(',', $id);
            foreach($ids as $k=>$v){
                //评论数
                $num=M('posts_comment')->where(array('pid'=>$v))->count();
                $num=$num+1;
                $psid=M('posts_comment')->where(array('id'=>$v,'pid'=>0))->getField('psId');
                //更新评论数
                if($psid){
                    M('posts')->where(array('id'=>$psid))->setDec('comNum',$num);
                }
               M('posts_comment')->where(array('pid'=>$v))->setField(array('isDel'=>1));
            }
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
        
    }
    /******************举报管理****************/
    //查看举报列表
    public function Report(){
        $where=" where i.isDel=0";
        $icontent=I('icontent');
        $status=I('status','-1',intval);
        $title = I('title');
        $pcontent = I('pcontent');
        $userName = I('userName');
        if($icontent){
            switch($icontent){
                case 1: $where.=" and  i.content like '%欺诈%'";break;
                case 2: $where.=" and  i.content like '%色情%'";break;
                case 3: $where.=" and  i.content like '%政治谣言%'";break;
                case 4: $where.=" and i.content like '%常识性谣言%'";break;
                case 5: $where.=" and  i.content like '%恶意营销%'";break;
                case 6: $where.=" and  i.content like '%隐私信息收集%'";break;
                case 7: $where.=" and  i.content like '%其他%'";break;
                case 8: $where.=" and i.content like '%原创%'";break;
            }
        }
        if($status>=0){$where.=" and i.status = $status";}
        if($title!=''){$where .= " and p.title like '%$title%'";} 
        if($pcontent!=''){$where .= " and p.content like '%$pcontent%'";}
        if($userName != ''){$where .= " and (u.userName like '%$userName%' or u.userName like '%$userName%')";}
        $sql="select i.id, i.psId,i.time as itime,i.content as icontent ,i.status as istatus,i.userId as iuserId ,p.title,p.userId as puserId,p.content as pcontent ,p.id as pid,t.name as cateName,u.userName,u.userName,iu.userName as iuserName,iu.userName as iuserName from oto_posts_inform as i  left join oto_posts as p on p.id=i.psId left join oto_posts_cate as t on t.cid=p.cid left join oto_users as u on u.userId=p.userId left join oto_users as iu on iu.userId=i.userId $where order by i.id desc";
        $info = $this->pageQuery($sql);
        return $info;
    }
    //修改举报处理状态
    public  function editReportStatus(){
        $postsInModel = M('posts_inform');
        $rd = array('status'=>-1);
        if(I('id',0)==0)return $rd;
        $status = (int)I('status');
        $id = (int)I('id');
        $postsInModel->status = ($status==1)?1:0;
        $rs = $postsInModel->where("id = {$id}")->save();
        if(false !== $rs){
            $rd['status']= 0;
        }
        return $rd;
    }
    //删除举报
    public  function delInform(){
        $id=I('id');
        if(!$id){
            return  array('status'=>-1);
        }
        $res=M('posts_inform')->where(array('id'=>$id))->setField(array('isDel'=>1));
        if($res!=false){
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
    }
    //批量删除举报
    public  function delSelectInform(){
        $id = self::formatIn(",", I('id',0));
        if(!$id){
            return  array('status'=>-1);
        }
        $postsInModel = M('posts_inform');
        $res=$postsInModel->where("id in({$id})")->setField(array('isDel'=>1));
        if($res!=false){
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }  
    }
    //批量未处理帖子
    public  function hideSelectInform(){
        $postsInModel = M('posts_inform');
        $id = self::formatIn(",", I('id',0));
        if(!$id){
            return  array('status'=>-1);
        }
        $postsInModel->status = 0;
        $res=$postsInModel->where("id in({$id})")->save();
        if($res!=false){
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
    
    }
    //批量已处理帖子
    public  function showSelectInform(){
        $postsInModel = M('posts_inform');
        $id = self::formatIn(",", I('id',0));
        if(!$id){
            return  array('status'=>-1);
        }
        $postsInModel->status = 1;
        $res=$postsInModel->where("id in({$id})")->save();
        if($res!=false){
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
    }
};
?>