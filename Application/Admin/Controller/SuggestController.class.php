<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/12/10
 * Time: 11:12
 */
namespace Admin\Controller;
class SuggestController extends BaseController
{
    /**
     * 显示反馈意见列表
     */
    public function index()
    {
        $this->isLogin();
        $su = D('Suggest');
        $page = $su->_select();
        $pager = new \Think\Page($page['total'],$page['pageSize']);// 实例化分页类 传入总记录数和每页显示的记录数
        $page['pager'] = $pager->show();
        $this->assign('Page',$page);
        $this->assign('list',$data['list']);
        $this->assign('page',$data['show']);
        $this->view->display('Suggest/index');
    }

    /**
     * 根据评论ID删除该评分
     */
    public function delete()
    {
        $this->isLogin();
        $su = D('Suggest');
        $res = $su->_delete();
        $this->ajaxReturn($res);
    }
}